<?php

namespace App\Mail;

use App\Penghasilan;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Penghasilaninvoice extends Mailable
{
    use Queueable, SerializesModels;
    public $penghasilan;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($trans)
    {
        $this->penghasilan = $trans;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('miftahul1220@gmail.com')
                    ->subject('Nota Pembayaran Zakat Penghasilan')
                    ->view('penghasilan.invoice-penghasilan');
    }
}
