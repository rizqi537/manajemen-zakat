<?php

namespace App\Mail;

use App\Fidyah;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Fidyahinvoice extends Mailable
{
    use Queueable, SerializesModels;
    public $fidyah;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($trans)
    {
        $this->fidyah = $trans;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('miftahul1220@gmail.com')
                    ->subject('Nota Pembayaran Fidiah')
                    ->view('fidyah.invoice-fidyah');
    }
}
