<?php

namespace App\Mail;

use App\Infaq;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Infaqinvoice extends Mailable
{
    use Queueable, SerializesModels;
    public $infaq;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($trans)
    {
        $this->infaq = $trans;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('miftahul122@gmail.com')
                    ->subject('Nota Pembayaran Infaq')
                    ->view('infaq.invoice-infaq');
    }
}
