<?php

namespace App\Mail;

use App\Fitrah;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Fitrahinvoice extends Mailable
{
    use Queueable, SerializesModels;
    public $fitrah;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($trans)
    {
        $this->fitrah = $trans;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('miftahul1220@gmail.com')
                    ->subject('Nota Pembayaran Zakat Fitrah')
                    ->view('fitrah.invoice-fitrah');
    }
}
