<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengeluaran extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'typezakat_id', 'typemustahiq_id', 'jumlah', 'keterangan', 'user_id', 'bukti',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function typezakat()
    {
        return $this->belongsTo(Typezakat::class);
    }

    public function jenismustahiq()
    {
        return $this->belongsTo(JenisMustahiq::class, 'typemustahiq_id', 'id');
    }
}
