<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fitrah extends Model
{
    protected $fillable = [
        'harga_beras', 'jumlah_jiwa', 'totalfit', 'jumlah_infaq', 'nama', 'nohp', 'email', 'zakat_infaq',
    ];

    public function fitrah()
    {
        return $this->hasMany(Fitrah::class, 'id');
    }
    public static function getmanage()
    {
        $query = Fitrah::all();
        return $query;
    }
}
