<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infaq extends Model
{
    protected $fillable = [
        'totalfaq','nama','nohp','email','bukti','keterangan',
    ];

    public function infaq()
    {
        return $this->hasMany(Infaq::class, 'id');
    }
    public static function getmanage()
    {
        $query = Infaq::all();
        return $query;
    }
}
