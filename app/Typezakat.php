<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Typezakat extends Model
{
    protected $fillable = [
        'nama_zakat','total_zakat',
    ];

    public function typezakat()
    {
        return $this->hasMany(Typezakat::class, 'id');
    }

    public static function getmanage()
    {
        $query = Typezakat::all();
        return $query;
    }
}
