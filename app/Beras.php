<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beras extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uang_beras', 
    ];

    public function beras()
    {
        return $this->hasMany(Beras::class, 'id');
    }

    public static function getmanage()
    {
        $query = Beras::all();
        return $query;
    }

    public static function getberas()
    {
        $query = Beras::select('kg','uang_beras')->where('id', $request->id)->first();

        return $query;
    }
}
