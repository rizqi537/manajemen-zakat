<?php

namespace App\Http\Controllers;

use App\Pengeluaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Typezakat;
use Image;
use App\JenisMustahiq;

class PengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $typezakat = Typezakat::getmanage();
        $jenismustahiq = JenisMustahiq::getmanage();
        return view('pengeluaran.index', compact('typezakat', 'jenismustahiq'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->file('bukti')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('bukti')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('bukti')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('bukti')->storeAs('uploadpengeluaran', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $pengeluaran = new Pengeluaran;
        $pengeluaran->typezakat_id = $request->typezakat_id;
        $pengeluaran->typemustahiq_id = $request->typemustahiq_id;


        $typezakat = Typezakat::findOrfail($request->typezakat_id);
        if ($typezakat->total_zakat > 0) {
            $pengeluaran->jumlah = $request->input('jumlah');
            $typezakat->total_zakat -= $request->jumlah;
            $typezakat->save();

            $pengeluaran->keterangan = $request->input('keterangan');
            $pengeluaran->user_id = Auth::user()->id;
            $pengeluaran->bukti = $fileNameToStore;
            $pengeluaran->nama = $request->input('nama');
            $pengeluaran->alamat = $request->input('alamat');
            $pengeluaran->nik = $request->input('nik');
            $pengeluaran->pekerjaan = $request->input('pekerjaan');
            $pengeluaran->status = $request->input('status');
            $pengeluaran->jumlah_mustahik = $request->input('jumlah_mustahik');
            $pengeluaran->save();
        } else {
            return redirect()->back()->withDanger('Dana Zakat Minus');
        }

        $pengeluaran->keterangan = $request->input('keterangan');
        $pengeluaran->user_id = Auth::user()->id;
        $pengeluaran->bukti = $fileNameToStore;
        $pengeluaran->save();

        /*Pengeluaran::create([
            'typezakat_id' => $request->typezakat_id,
            'typemustahiq_id' => $request->typemustahiq_id,
            'jumlah' => $request->jumlah,
            'keterangan' => $request->keterangan,
            'user_id' => Auth::user()->id
        ]);*/

        return redirect()->back()->withSuccess('Data Berhasil Ditambahkan');
    }




    /**
     * Display the specified resource.
     *
     * @param  \App\Pengeluaran  $pengeluaran
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $pengeluarans = Pengeluaran::select(['pengeluarans.id', 'users.name as nama', 'pengeluarans.jumlah', 'pengeluarans.keterangan', 'typezakats.nama_zakat as nama_zakat', 'jenis_mustahiqs.jenis as jenis'])
            ->join('users', 'pengeluarans.user_id', '=', 'users.id')
            ->join('typezakats', 'pengeluarans.typezakat_id', '=', 'typezakats.id')
            ->join('jenis_mustahiqs', 'pengeluarans.typemustahiq_id', '=', 'jenis_mustahiqs.id')
            ->orderBy('pengeluarans.id');

        return DataTables::of($pengeluarans)
            ->addcolumn('action', function ($pengeluarans) {
                return '<a title="Detil Data" id="myform" href="' . url('edit-pengeluaran') . "/" . base64_encode($pengeluarans->id) . '" class="btn btn-xs btn-primary" ><i class="material-icons">pageview</i></a>';
            })
            ->addColumn('jumlah', function ($pengeluarans) {
                return 'Rp. ' . number_format($pengeluarans->jumlah, 0, '', ',');
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pengeluaran  $pengeluaran
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $idpengeluaran = base64_decode($id);
        $pengeluaran = Pengeluaran::findOrfail($idpengeluaran);
        $typezakat = Typezakat::all();
        $jenismustahiq = JenisMustahiq::getmanage();
        return view('pengeluaran.view-pengeluaran', compact('pengeluaran', 'typezakat', 'jenismustahiq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pengeluaran  $pengeluaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //        $idpengeluaran = base64_decode($id);
        $pengeluaran = Pengeluaran::findOrfail($id);
        $pengeluaran->jumlah = $request->jumlah;
        $pengeluaran->keterangan = $request->keterangan;
        $pengeluaran->save();

        return redirect()->back()->withSuccess('Data Pengeluaran Untuk ' . $pengeluaran->keterangan . ' Berhasil Dirubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pengeluaran  $pengeluaran
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idpengeluaran = base64_decode($id);
        $pengeluaran = Pengeluaran::findOrfail($idpengeluaran);
        $pengeluaran->delete();
    }
}
