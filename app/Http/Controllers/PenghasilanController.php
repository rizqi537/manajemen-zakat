<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Response;
use App\Penghasilan;
use App\Beras;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Barryvdh\DomPDF\Facade as PDF;
use App\Mail\Penghasilaninvoice;
use App\Typezakat;

class PenghasilanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['store', 'showinsertpenghasilan', 'bukti','show_insert']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penghasilans = Penghasilan::all();
        return view('penghasilan.penghasilan-list');
    }

    public function getpenghasilans()
    {
        $penghasilans = Penghasilan::getmanage();
        return Datatables::of($penghasilans)

            ->addcolumn('action', function ($penghasilans) 
            { if($penghasilans->keterangan =='Belum Dikonfirmasi'){
                return '<a title="Rubah Data" id="myform" href="' . url('edit-penghasilan') . "/" . base64_encode($penghasilans->id) . '" class="btn btn-xs btn-primary" ><i class="material-icons">border_color</i></a>'
                    . '<a title="Konfirmasi" id="myform" href="' . url('invoice-penghasilan') . "/" . base64_encode($penghasilans->id) . '" class="btn btn-xs btn-primary" ><i class="material-icons">add_circler</i></a>'
                    . '<a title="Hapus Data" id="apus" data-value="' . base64_encode($penghasilans->id) . '" class="btn btn-xs btn-danger" ><i class="material-icons">delete</i></a>';
            } else {
                    return '<a title="Hapus Data" id="apus" data-value="' . base64_encode($penghasilans->id) . '" class="btn btn-xs btn-danger" ><i class="material-icons">delete</i></a>';
            }
                
            })
            ->addColumn('gaji_bulan', function ($penghasilans) {
                return 'Rp. ' . number_format($penghasilans->gaji_bulan, 0, '', ',');
            })
            ->addColumn('harga_beras', function ($penghasilans) {
                return 'Rp. ' . number_format($penghasilans->harga_beras, 0, '', ',');
            })
            ->addColumn('jumlah_zakat', function ($penghasilans) {
                return 'Rp. ' . number_format($penghasilans->jumlah_zakat, 0, '', ',');
            })

            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'harga_beras'=>'required',
            'gaji_bulan'=>'required',
            'jumlah_zakat'=>'required',
            'nama'=>'required',
            'nohp'=>'required',
            'email'=>'required'
        ]);

        $trans = Penghasilan::create([
            'harga_beras' => $request->harga_beras,
            'gaji_bulan' => $request->gaji_bulan,
            'jumlah_zakat' => $request->jumlah_zakat,
            'nama' => $request->nama,
            'nohp' => $request->nohp,
            'email' => $request->email
        ]);

        $idpenghasilan = base64_encode($trans->id);
        Alert::toast('Berhasil', 'success');
        return redirect('confirmationincome/' . $idpenghasilan);
    }

    public function show_insert(Request $request)
    {
        $cari = $request->get('nohp');

        $penghasilan = DB::table('penghasilans')
            ->where('nohp','like',"%".$cari."%")->first();

        return view('penghasilan.konfirmasi-bayar',['penghasilan' => $penghasilan]);
    }

    public function showinsertpenghasilan($id)
    {
        $idtranspenghasilan = base64_decode($id);
        $penghasilan = Penghasilan::findOrfail($idtranspenghasilan);

        return view('penghasilan.konfirmasi-bayar')->with('penghasilan', $penghasilan);
    }

    public function bukti(Request $request, $id)
    {

        $this->validate($request,[
            'bukti'=>'required'
        ]);


        // Handle File Upload
        if ($request->file('bukti')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('bukti')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('bukti')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('bukti')->storeAs('uploadpenghasilan', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $penghasilan = Penghasilan::find($id);
        $penghasilan->bukti = $fileNameToStore;
        $penghasilan->save();
        Alert::toast('Berhasil', 'success');
        return redirect('/form-penghasilan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = base64_decode($id);
        $penghasilan = Penghasilan::findOrfail($id);
        $beras = Beras::all();
        return view('penghasilan.edit-penghasilan', compact('beras'))->with('penghasilan', $penghasilan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // Handle File Upload
        if ($request->file('bukti')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('bukti')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('bukti')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('bukti')->storeAs('uploadpenghasilan', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $penghasilan = Penghasilan::findOrfail(request('id'));
        $penghasilan->harga_beras = request('harga_beras');
        $penghasilan->gaji_bulan = request('gaji_bulan');
        $penghasilan->jumlah_zakat = request('jumlah_zakat');
        $penghasilan->nama = request('nama');
        $penghasilan->nohp = request('nohp');
        $penghasilan->email = request('email');
        if ($request->hasFile('bukti')) {
            $penghasilan->bukti = $fileNameToStore;
        }
        $penghasilan->save();

        return redirect()->back()->withSuccess('Data Zakat penghasilan Berhasil Dirubah');
    }

    public function createPDF($id)
    {
        $id_penghasilan = base64_decode($id);
        $penghasilan = Penghasilan::findOrFail($id_penghasilan);
        $zakatType = Typezakat::findOrFail($penghasilan->tipe_zakat);
        if ($zakatType) {
            $oldTotalZakat = $zakatType->total_zakat; // temporary data total zakat
            $newTotalZakat = $oldTotalZakat + $penghasilan->jumlah_zakat; // calculation total zakat
            $zakatType->update(['total_zakat' => $newTotalZakat]);
            $penghasilan->update(['keterangan' => 'Telah Dikonfirmasi']);
        }

         $id_penghasilan = base64_decode($id);
         $penghasilan = Penghasilan::find($id_penghasilan)->where('id', $id_penghasilan)->update(['keterangan' => 'Telah Dikonfirmasi']);
         $penghasilan = Penghasilan::findOrfail($id_penghasilan);
         $val = array($penghasilan->harga_beras, $penghasilan->jumlah_zakat, $penghasilan->nama, $penghasilan->nohp, $penghasilan->email);
         $data = array_sum($val);

        if ($penghasilan->email != "-" && $penghasilan->email != NULL) {
            Mail::to($penghasilan->email)->send(new Penghasilaninvoice($penghasilan));
        }

        return redirect()->back()->withSuccess('Data penghasilan telah di konfirmasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idpenghasilan = base64_decode($id);
        $penghasilan = Penghasilan::findOrfail($idpenghasilan);
        $penghasilan->delete();
    }
}
