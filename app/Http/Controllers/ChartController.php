<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Fidyah;
use App\Fitrah;
use App\Penghasilan;
use App\Infaq;
use App\Typezakat;

class ChartController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getdataboard()
    {
        $typezakat = DB::table('typezakats')
                ->selectRaw("SUM(total_zakat) AS JumlahZakat")
                ->first();
        
        $zfitrah = Typezakat::all()->where('id','1')->first();
        $zfidyah = Typezakat::all()->where('id','2')->first();
        $zpenghasilan = Typezakat::all()->where('id','3')->first();
        $zinfaq = Typezakat::all()->where('id','4')->first();

        /*$fitrah = DB::table('fitrahs')
                ->selectRaw("SUM(totalfit) AS Totalfit")
                ->first();
        
        $penghasilan = DB::table('penghasilans')
                        ->selectRaw("SUM(jumlah_zakat) AS JumlahZakat")
                        ->first();*/

        
        $muzzaki1 = Fitrah::groupBy('nama')->count();
        $muzzaki2 = Penghasilan::groupBy('nama')->count();
        $muzzaki3 = Fidyah::groupBy('nama')->count();
        $muzzaki4 = Infaq::groupBy('nama')->count();
        $data = $muzzaki1+$muzzaki2+$muzzaki3+$muzzaki4;

        $report1 = DB::table('pengeluarans')->where('typezakat_id','1')->sum('jumlah');
        $report2 = DB::table('pengeluarans')->where('typezakat_id','2')->sum('jumlah');
        $report3 = DB::table('pengeluarans')->where('typezakat_id','3')->sum('jumlah');
        $report4 = DB::table('pengeluarans')->where('typezakat_id','4')->sum('jumlah');
        $totalreport = $report1+$report2+$report3+$report4;
                        
        
        return view('board2', compact('typezakat','data','zfitrah','zfidyah','zpenghasilan','zinfaq','report1','report2','report3','report4','totalreport'));
    }
}
