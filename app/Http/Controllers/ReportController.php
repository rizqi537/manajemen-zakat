<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Transaksi;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('report.report-form');
    }

    public function index2()
    {
        return view('report.report-form3');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tanggalReport = $request->tanggal_report;
       
        $report = DB::table('transaksis')->whereDate('created_at', $request->tanggal_report)
        ->selectRaw("SUM(jiwa) AS Jiwa, SUM(beras_fitrah) AS Beras, SUM(uang_fitrah) AS Uang, SUM(fidyah) AS Fidyah, SUM(zakat_maal) AS Maal, SUM(infaq) AS Infaq")
        ->first();
        
        return view('report.report-template',compact('report','tanggalReport'));
    }

    public function make(Request $request)
    {
        $tanggalReport = $request->tanggal_report;
        $tanggalReport2 = $request->tanggal_report2;

        $report1 = DB::table('pengeluarans')->whereBetween('created_at', [$tanggalReport. ' 00:00:00',$tanggalReport2. ' 23:59:59'])->where('typezakat_id','1')->sum('jumlah');
        $report2 = DB::table('pengeluarans')->whereBetween('created_at', [$tanggalReport. ' 00:00:00',$tanggalReport2. ' 23:59:59'])->where('typezakat_id','2')->sum('jumlah');
        $report3 = DB::table('pengeluarans')->whereBetween('created_at', [$tanggalReport. ' 00:00:00',$tanggalReport2. ' 23:59:59'])->where('typezakat_id','3')->sum('jumlah');
        $report4 = DB::table('pengeluarans')->whereBetween('created_at', [$tanggalReport. ' 00:00:00',$tanggalReport2. ' 23:59:59'])->where('typezakat_id','4')->sum('jumlah');

        $totalreport = $report1+$report2+$report3+$report4;
        
        return view('report.report-template2', compact('report1','report2','report3','totalreport','report4','tanggalReport'));
    }

    public function make2(Request $request)
    {
        $tanggalReport = $request->tanggal_report;
        $tanggalReport2 = $request->tanggal_report2;

        $report1 = DB::table('fitrahs')->whereBetween('updated_at', [$tanggalReport. ' 00:00:00',$tanggalReport2. ' 23:59:59'])->where('keterangan','Telah Dikonfirmasi')->sum('totalfit');
        $report2 = DB::table('fidyahs')->whereBetween('updated_at', [$tanggalReport. ' 00:00:00',$tanggalReport2. ' 23:59:59'])->where('keterangan','Telah Dikonfirmasi')->sum('totalfi');
        $report3 = DB::table('penghasilans')->whereBetween('updated_at', [$tanggalReport. ' 00:00:00',$tanggalReport2. ' 23:59:59'])->where('keterangan','Telah Dikonfirmasi')->sum('jumlah_zakat');
        $report4 = DB::table('infaqs')->whereBetween('updated_at', [$tanggalReport. ' 00:00:00',$tanggalReport2. ' 23:59:59'])->where('keterangan','Telah Dikonfirmasi')->sum('totalfaq');

        $totalreport = $report1+$report2+$report3+$report4;

        return view('report.report-template3', compact('report1','report2','report3','totalreport','report4','tanggalReport'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = DB::table('transaksis')->whereYear('created_at', date('Y'))
        ->selectRaw("SUM(jiwa) AS Jiwa, SUM(beras_fitrah) AS Beras, SUM(uang_fitrah) AS Uang, SUM(fidyah) AS Fidyah, SUM(zakat_maal) AS Maal, SUM(infaq) AS Infaq")
        ->first();
        
        return view('report.general-report',compact('report'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
