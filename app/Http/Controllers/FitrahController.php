<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Response;
use Illuminate\Http\Request;
use App\Fitrah;
use App\Typezakat;
use App\Mail\Fitrahinvoice;
use App\Beras;
use Barryvdh\DomPDF\Facade as PDF;
use Mail;
use RealRashid\SweetAlert\Facades\Alert;

class FitrahController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['store', 'showinsertfitrah', 'bukti', 'show_insert']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fitrahs = Fitrah::getmanage();
        return view('fitrah.fitrah-list');
    }

    public function getfitrahs()
    {
        $fitrahs = Fitrah::getmanage();
        return Datatables::of($fitrahs)
            ->addcolumn('action', function ($fitrahs) {
                if ($fitrahs->keterangan == 'Belum Dikonfirmasi') {
                    return '<a title="Rubah Data" id="myform" href="' . url('edit-fitrah') . "/" . base64_encode($fitrahs->id) . '" class="btn btn-xs btn-primary" ><i class="material-icons">border_color</i></a>'
                        . '<a title="Konfirmasi Data" id="myform" href="' . url('invoice-fitrah') . "/" . base64_encode($fitrahs->id) . '" class="btn btn-xs btn-primary" ><i class="material-icons">check</i></a> <br>'
                        . '<a title="Hapus Data" id="apus" data-value="' . base64_encode($fitrahs->id) . '" class="btn btn-xs btn-danger" ><i class="material-icons">delete</i></a>';
                } else {
                    return '<a title="Hapus Data" id="apus" data-value="' . base64_encode($fitrahs->id) . '" class="btn btn-xs btn-danger" ><i class="material-icons">delete</i></a>';
                }
            })
            ->addColumn('harga_beras', function ($fitrahs) {
                return 'Rp. ' . number_format($fitrahs->harga_beras, 0, '', ',');
            })
            ->addColumn('totalfit', function ($fitrahs) {
                return 'Rp. ' . number_format($fitrahs->totalfit, 0, '', ',');
            })

            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'harga_beras' => 'required',
            'jumlah_jiwa' => 'required',
            'totalfit' => 'required',
            'nama' => 'required',
            'nohp' => 'required|min:3|unique:fitrahs',
            'email' => 'required'
        ]);

        $trans = Fitrah::create([
            'harga_beras' => $request->harga_beras,
            'jumlah_jiwa' => $request->jumlah_jiwa,
            'totalfit' => $request->totalfit,
            'jumlah_infaq' => $request->jumlah_infaq,
            'nama' => $request->nama,
            'nohp' => $request->nohp,
            'email' => $request->email,
            'zakat_infaq' => $request->jumlah_infaq,
        ]);

        $idfitrah = base64_encode($trans->id);
        Alert::toast('Berhasil', 'success');
        return redirect('confirmationfitrah/' . $idfitrah);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {

        $id = base64_decode($id);
        $fitrah = Fitrah::findOrfail($id);
        $beras = Beras::all();
        return view('fitrah.edit-fitrah', compact('beras'))->with('fitrah', $fitrah);
    }

    public function show_insert(Request $request)
    {
        $cari = $request->get('nohp');

        $fitrah = DB::table('fitrahs')
            ->where('nohp', 'like', "%" . $cari . "%")->first();

        return view('fitrah.konfirmasi', ['fitrah' => $fitrah]);
    }

    public function showinsertfitrah($id)
    {
        $idtransfitrah = base64_decode($id);
        $fitrah = Fitrah::findOrfail($idtransfitrah);

        return view('fitrah.konfirmasi')->with('fitrah', $fitrah);
    }

    public function bukti(Request $request, $id)
    {
        $this->validate($request, [
            'bukti' => 'required'
        ]);

        // Handle File Upload
        if ($request->file('bukti')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('bukti')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('bukti')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('bukti')->storeAs('uploadfitrah', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $fitrah = Fitrah::find($id);
        if ($request->hasFile('bukti')) {
            $fitrah->bukti = $fileNameToStore;
        }
        $fitrah->save();
        Alert::toast('Berhasil', 'success');
        return redirect('/zakatfitrah');
    }

    public function createPDF($id)
    {
        $id_fitrah = base64_decode($id);
        $fitrah = Fitrah::findOrFail($id_fitrah);
        $zakatType = Typezakat::findOrFail($fitrah->tipe_zakat);
        if ($zakatType) {
            if ($fitrah->zakat_infaq) {
                $zakat_infaq = Typezakat::find(4);
                $oldTotalZakatInfaq = $zakat_infaq->total_zakat; // temporary data total zakat
                $newTotalZakatInfaq = $oldTotalZakatInfaq + $fitrah->zakat_infaq; // calculation total zakat
                $zakat_infaq->update(['total_zakat' => $newTotalZakatInfaq]);
            }
            $oldTotalZakat = $zakatType->total_zakat; // temporary data total zakat
            $newTotalZakat = $oldTotalZakat + $fitrah->totalfit; // calculation total zakat
            $zakatType->update(['total_zakat' => $newTotalZakat]);
            $fitrah->update(['keterangan' => 'Telah Dikonfirmasi']);
        }

        $id_fitrah = base64_decode($id);
        $fitrah = Fitrah::find($id_fitrah)->where('id', $id_fitrah)->update(['keterangan' => 'Telah Dikonfirmasi']);

        $fitrah = Fitrah::findOrfail($id_fitrah);
        $val = array($fitrah->uang_beras, $fitrah->jumlah_jiwa, $fitrah->totalfit, $fitrah->nama, $fitrah->nohp, $fitrah->email);
        $data = array_sum($val);

        if ($fitrah->email != "-" && $fitrah->email != NULL) {
            Mail::to($fitrah->email)->send(new Fitrahinvoice($fitrah));
        }

        return redirect()->back()->withSuccess('Data Fitrah telah di konfirmasi');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($request->file('bukti')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('bukti')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('bukti')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('bukti')->storeAs('uploadfitrah', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $fitrah = Fitrah::findOrfail(request('id'));
        $fitrah->harga_beras = request('harga_beras');
        $fitrah->jumlah_jiwa = request('jumlah_jiwa');
        $fitrah->totalfit = request('totalfit');
        $fitrah->nama = request('nama');
        $fitrah->nohp = request('nohp');
        $fitrah->email = request('email');
        if ($request->hasFile('bukti')) {
            $fitrah->bukti = $fileNameToStore;
        }
        $fitrah->save();

        return redirect()->back()->withSuccess('Data Zakat Fitrah Berhasil Dirubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idfitrah = base64_decode($id);
        $fitrah = Fitrah::findOrfail($idfitrah);
        $fitrah->delete();
    }
}
