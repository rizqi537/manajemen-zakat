<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Yajra\Datatables\Datatables;
use App\Artikel;
use Image;
use RealRashid\SweetAlert\Facades\Alert;

class ArtikelController extends Controller
{

    

    public function __construct()
    {
        $this->middleware('auth',['except'=>['view']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artikel = Artikel::all();
        return view('artikel.manage-artikel');
    }

    public function getartikeldata()
    {
        $artikelss = Artikel::getmanage();
        return Datatables::of($artikelss)
        ->addcolumn('action', function($artikelss){
            return '<a title="Rubah Data" id="myform" href="'. url('edit-artikel')."/".base64_encode($artikelss->id) .'" class="btn btn-xs btn-primary" ><i class="material-icons">border_color</i></a>'
            .'<a title="Hapus Data" id="apus" data-value="'.base64_encode($artikelss->id).'" class="btn btn-xs btn-danger" ><i class="material-icons">delete</i></a>';  
        })            
        ->editColumn('id', 'ID: {{$id}}')
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Handle File Upload
        if($request->file('photo')){
            // Get filename with the extension
            $filenameWithExt = $request->file('photo')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('photo')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('photo')->storeAs('uploadphoto', $fileNameToStore);
            
        } else {
            $fileNameToStore = 'noimage.jpg';
        }
        // Create Article
        $artikel = new Artikel;
        $artikel->title = $request->input('title');
        $artikel->description = $request->input('description');
        $artikel->photo = $fileNameToStore;
        $artikel->save();
        Alert::toast('Article Created', 'success');
        return redirect('/artikel')->with('success','Article Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $idartikel = base64_decode($id);
        $artikel = Artikel::findOrfail($idartikel);
        return view('artikel.edit-artikel')->with('artikel', $artikel);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Handle File Upload
        if($request->file('photo')){
            // Get filename with the extension
            $filenameWithExt = $request->file('photo')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('photo')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('photo')->storeAs('uploadphoto', $fileNameToStore);
            
        } else {
            $fileNameToStore = 'noimage.jpg';
        }


        $artikel = Artikel::findOrfail(request('id'));
        $artikel->title = $request->input('title');
        $artikel->description = $request->input('description');
        if($request->hasFile('photo')){
            $artikel->photo = $fileNameToStore;
        }

        $artikel->save();
        Alert::toast('Article Created', 'success');
        return redirect('/artikel')->with('success','Article Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idartikel = base64_decode($id);
        $artikel = Artikel::findOrfail($idartikel);
        $artikel->delete();
    }

    public function view(Request $request, $id)
    {
        $artikel = Artikel::findOrfail($id);
        return view('artikel.view-artikel')->with('artikel', $artikel);
    }
}
