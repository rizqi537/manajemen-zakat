<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Response;
use App\Typezakat;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Barryvdh\DomPDF\Facade as PDF;
use App\Mail\Penghasilaninvoice;

class TypezakatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $typezakat = Typezakat::all();
        return view('pengeluaran_zakat.list-outzakat', compact('typezakat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $typezakat = new Typezakat;
        $typezakat->nama_zakat = $request->input('nama_zakat');
        $typezakat->save();
        Alert::toast('Data Zakat Saved', 'success');
        return redirect('/typezakat')->with('success','Data Zakat Saved');
    }

    public function getallzakat()
    {
        $typezakatss = Typezakat::all();
        return Datatables::of($typezakatss)
        ->addcolumn('action', function($typezakatss){
              
        })
        ->addColumn('total_zakat', function ($typezakatss) {
            return 'Rp. ' . number_format($typezakatss->total_zakat, 0, '', ',');
        })            
        ->editColumn('id', 'ID: {{$id}}')
        ->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
