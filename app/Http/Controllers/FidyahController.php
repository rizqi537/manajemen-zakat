<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Response;
use Illuminate\Http\Request;
use App\Fidyah;
use Barryvdh\DomPDF\Facade as PDF;
use App\Mail\Fidyahinvoice;
use App\Typezakat;
use Mail;
use RealRashid\SweetAlert\Facades\Alert;
use Intervention\Image\Facades\Image;

class FidyahController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth', ['except' => ['store', 'bukti', 'showinsertfidyah','show_insert']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fidyahs = Fidyah::getmanage();
        return view('fidyah.fidyah-list');
    }

    public function getFidyahs()
    {
        $fidyahs = Fidyah::getmanage();
        return Datatables::of($fidyahs)
            ->addcolumn('action', function ($fidyahs) 
            { if($fidyahs->keterangan == 'Belum Dikonfirmasi'){
                return '<a title="Rubah Data" id="myform" href="' . url('tampilkan-fidyah') . "/" . base64_encode($fidyahs->id) . '" class="btn btn-xs btn-primary" ><i class="material-icons">border_color</i></a>'
                    . '<a title="Konfirmasi" id="myform" href="' . url('invoice-fidyah') . "/" . base64_encode($fidyahs->id) . '" class="btn btn-xs btn-primary" ><i class="material-icons">check</i></a>'
                    . '<a title="Hapus Data" id="apus" data-value="' . base64_encode($fidyahs->id) . '" class="btn btn-xs btn-danger" ><i class="material-icons">delete</i></a>';
            } else {
                return '<a title="Hapus Data" id="apus" data-value="' . base64_encode($fidyahs->id) . '" class="btn btn-xs btn-danger" ><i class="material-icons">delete</i></a>';
            }
                
            })
            ->addColumn('rupiah', function ($fidyahs) {
                return 'Rp. ' . number_format($fidyahs->rupiah, 0, '', ',');
            })
            ->addColumn('totalfi', function ($fidyahs) {
                return 'Rp. ' . number_format($fidyahs->totalfi, 0, '', ',');
            })

            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'jumlahhari'=>'required',
            'totalfi'=>'required',
            'nama'=>'required',
            'nohp'=>'required',
            'email'=>'required'
        ]);

        $trans = Fidyah::create([
            'jumlahhari' => $request->jumlahhari,
            'totalfi' => $request->totalfi,
            'nama' => $request->nama,
            'nohp' => $request->nohp,
            'email' => $request->email
        ]);

        $idfidyah = base64_encode($trans->id);
        Alert::toast('Berhasil', 'success');
        return redirect('confirmationfidyah/' . $idfidyah);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $id = base64_decode($id);
        $fidyah = Fidyah::findOrfail($id);
        return view('fidyah.edit-fidyah')->with('fidyah', $fidyah);
    }

    public function show_insert(Request $request)
    {
        $cari = $request->get('nohp');

        $fidyah = DB::table('fidyahs')
            ->where('nohp','like',"%".$cari."%")->first();

        return view('fidyah.konfirmasi-fidyah',['fidyah' => $fidyah]);
    }

    public function showinsertfidyah($id)
    {
        $idtransfidyah = base64_decode($id);
        $fidyah = Fidyah::findOrfail($idtransfidyah);

        return view('fidyah.konfirmasi-fidyah')->with('fidyah', $fidyah);
    }

    public function createPDF($id)
    {
        $id_fidyah = base64_decode($id);
        $fidyah = Fidyah::findOrFail($id_fidyah);
        $zakatType = Typezakat::findOrFail($fidyah->tipe_zakat);
        if ($zakatType) {
            $oldTotalZakat = $zakatType->total_zakat; // temporary data total zakat
            $newTotalZakat = $oldTotalZakat + $fidyah->totalfi; // calculation total zakat
            $zakatType->update(['total_zakat' => $newTotalZakat]);
            $fidyah->update(['keterangan' => 'Telah Dikonfirmasi']);
        }

         $id_fidyah = base64_decode($id);
         $fidyah = Fidyah::find($id_fidyah)->where('id', $id_fidyah)->update(['keterangan' => 'Telah Dikonfirmasi']);
         $fidyah = Fidyah::findOrfail($id_fidyah);
         $val = array($fidyah->jumlahhari, $fidyah->totalfi, $fidyah->nama, $fidyah->nohp, $fidyah->email);
         $data = array_sum($val);

        if ($fidyah->email != "-" && $fidyah->email != NULL) {
            Mail::to($fidyah->email)->send(new Fidyahinvoice($fidyah));
        }

        return redirect()->back()->withSuccess('Data Fidyah telah di konfirmasi');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($request->file('bukti')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('bukti')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('bukti')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('bukti')->storeAs('uploadbukti', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $fidyah = Fidyah::findOrfail(request('id'));
        $fidyah->jumlahhari = request('jumlahhari');
        $fidyah->totalfi = request('totalfi');
        $fidyah->nama = request('nama');
        $fidyah->nohp = request('nohp');
        $fidyah->email = request('email');
        if ($request->hasFile('bukti')) {
            $fidyah->bukti = $fileNameToStore;
        }
        $fidyah->save();

        return redirect()->back()->withSuccess('Data Fidyah Berhasil Dirubah');
    }

    public function bukti(Request $request, $id)
    {

        $this->validate($request,[
            'bukti'=>'required'
        ]);

        // Handle File Upload
        if ($request->file('bukti')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('bukti')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('bukti')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('bukti')->storeAs('uploadbukti', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $fidyah = Fidyah::find($id);
        $fidyah->bukti = $fileNameToStore;
        $fidyah->save();
        Alert::toast('Berhasil', 'success');
        return redirect('/zakatfidyah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idfidyah = base64_decode($id);
        $fidyah = Fidyah::findOrfail($idfidyah);
        $fidyah->delete();
    }
}
