<?php

namespace App\Http\Controllers;

use App\Beras;
use Yajra\Datatables\Datatables;
use Response;
use Illuminate\Http\Request;

class BerasController extends Controller
{

    

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berass = Beras::all();
        return view('beras.beras-list');
    }

    public function getBerasData()
    {
        $berass = Beras::getmanage();
        return Datatables::of($berass)
        ->addColumn('action', function ($berass){
            return '<a title="Rubah Data" id="myform" href="'. url('tampilkan-beras')."/".base64_encode($berass->id) .'" class="btn btn-xs btn-primary" ><i class="material-icons">border_color</i></a>'
            .'<a title="Hapus Data" id="apus" data-value="'.base64_encode($berass->id).'" class="btn btn-xs btn-danger" ><i class="material-icons">delete</i></a>';
                    
        })            
        ->addColumn('uang_beras', function ($berass) {
          return 'Rp. '.number_format($berass->uang_beras,0,'',',');
        })
        ->editColumn('id', 'ID: {{$id}}')
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Beras::create([
            'uang_beras' => $request->uang_beras
        ]);
        
        return redirect()->back()->withSuccess('Data Zakat Beras Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getberas(Request $request, $id)
    {
        $id = base64_decode($id);
        $beras = Beras::findOrfail($id);
        return view('beras.edit-beras')->with('beras', $beras);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $Beras = Beras::findOrfail(request('id'));
        $Beras->uang_beras = request('uang_beras');
        $Beras->save();

        return redirect()->back()->withSuccess('Data Beras Berhasil Dirubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idberas = base64_decode($id);
        $beras = Beras::findOrfail($idberas);
        $beras->delete();
    }
}
