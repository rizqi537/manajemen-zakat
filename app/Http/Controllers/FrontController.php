<?php

namespace App\Http\Controllers;

use App\Beras;
use App\Artikel;
use Response;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    
    public function firstpage()
    {
        $artikels = Artikel::getartikel();
        return view('front.firstpage')->with('artikels',$artikels);
    }

    public function about()
    {
        return view('front.about');
    }

    public function zakatfitrahpage()
    {
        $beras = Beras::getmanage();
        return view('front.zakatfitrah', compact('beras'));
    }

    public function beras(Request $request)
    {
        $beras = Beras::select('kg','uang_beras')->where('id', $request->id)->first();
        return response()->json($beras);
    }

    public function fidyah()
    {
        return view ('front.zakatfidyah');
    }

    public function infaq()
    {
        return view ('front.infaq');
    }

    public function penghasilan()
    {

        $beras = Beras::getmanage();
        return view ('front.penghasilan', compact('beras'));
    }

    public function kalkulator()
    {
        $beras = Beras::getmanage();
        return view ('front.kalkulatorzakat', compact('beras'));
    }

    public function buktifitrah()
    {
        return view ('fitrah.inputkonfirmasi');
    }

    public function buktifidiah()
    {
        return view ('fidyah.inputkonfirmasi');
    }

    public function buktiinfaq()
    {
        return view ('infaq.inputkonfirmasi');
    }

    public function buktipenghasilan()
    {
        return view ('penghasilan.inputkonfirmasi');
    }

}
