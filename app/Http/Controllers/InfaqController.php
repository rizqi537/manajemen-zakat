<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Infaq;
use App\Typezakat;
use Barryvdh\DomPDF\Facade as PDF;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use App\Mail\Infaqinvoice;

class InfaqController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['store', 'bukti', 'showinsertinfaq','show_insert']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infaqs = Infaq::getmanage();
        return view('infaq.infaq-list');
    }
    public function getinfaqs()
    {
        $infaqs = Infaq::getmanage();
        return Datatables::of($infaqs)
            ->addcolumn('action', function ($infaqs) {
                if($infaqs->keterangan == 'Belum Dikonfirmasi'){
                    return '<a title="Rubah Data" id="myform" href="' . url('edit-infaq') . "/" . base64_encode($infaqs->id) . '" class="btn btn-xs btn-primary" ><i class="material-icons">border_color</i></a>'
                    . '<a title="Konfirmasi" id="myform" href="' . url('invoice-infaq') . "/" . base64_encode($infaqs->id) . '" class="btn btn-xs btn-primary" ><i class="material-icons">check</i></a>'
                    . '<a title="Hapus Data" id="apus" data-value="' . base64_encode($infaqs->id) . '" class="btn btn-xs btn-danger" ><i class="material-icons">delete</i></a>';
                } else {
                    return  '<a title="Hapus Data" id="apus" data-value="' . base64_encode($infaqs->id) . '" class="btn btn-xs btn-danger" ><i class="material-icons">delete</i></a>';
                }
                
            })
            ->addColumn('totalfaq', function ($infaqs) {
                return 'Rp. ' . number_format($infaqs->totalfaq, 0, '', ',');
            })

            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'totalfaq'=>'required',
            'nama'=>'required',
            'nohp'=>'required',
            'email'=>'required'
        ]);

        $trans = Infaq::create([
            'totalfaq' => $request->totalfaq,
            'nama' => $request->nama,
            'nohp' => $request->nohp,
            'email' => $request->email
        ]);

        $idinfaq = base64_encode($trans->id);
        Alert::toast('Berhasil', 'success');
        return redirect('confirmationinfaq/' . $idinfaq);
        //return redirect('/zakatinfaq');
        
    }

    public function show_insert(Request $request)
    {
        $cari = $request->get('nohp');

        $infaq = DB::table('infaqs')
            ->where('nohp','like',"%".$cari."%")->first();

        return view('infaq.konfirmasi',['infaq' => $infaq]);
    }

    public function showinsertinfaq($id)
    {
        $idtransinfaq = base64_decode($id);
        $infaq = Infaq::findOrfail($idtransinfaq);

        return view('infaq.konfirmasi')->with('infaq', $infaq);
    }

    public function bukti(Request $request, $id)
    {

        $this->validate($request,[
            'bukti'=>'required'
        ]);

        // Handle File Upload
        if ($request->file('bukti')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('bukti')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('bukti')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('bukti')->storeAs('uploadinfaq', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $infaq = Infaq::find($id);
        $infaq->bukti = $fileNameToStore;
        $infaq->save();
        Alert::toast('Berhasil', 'success');
        return redirect('/zakatinfaq');
    }

    public function createPDF($id)
    {
        $id_infaq = base64_decode($id);
        $infaq = Infaq::findOrFail($id_infaq);
        $zakatType = Typezakat::findOrFail($infaq->tipe_zakat);
        if ($zakatType) {
            $oldTotalZakat = $zakatType->total_zakat; // temporary data total zakat
            $newTotalZakat = $oldTotalZakat + $infaq->totalfaq; // calculation total zakat
            $zakatType->update(['total_zakat' => $newTotalZakat]);
            $infaq->update(['keterangan' => 'Telah Dikonfirmasi']);
        }

         $id_infaq = base64_decode($id);
         $infaq = Infaq::find($id_infaq)->where('id', $id_infaq)->update(['keterangan' => 'Telah Dikonfirmasi']);
         
         $infaq = Infaq::findOrfail($id_infaq);
         $val = array($infaq->totalfaq, $infaq->nama, $infaq->nohp, $infaq->email);
         $data = array_sum($val);

        if ($infaq->email != "-" && $infaq->email != NULL) {
            Mail::to($infaq->email)->send(new Infaqinvoice($infaq));
        }

        return redirect()->back()->withSuccess('Data Infaq telah di konfirmasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = base64_decode($id);
        $infaq = Infaq::findOrfail($id);
        return view('infaq.edit-infaq')->with('infaq', $infaq);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->file('bukti')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('bukti')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('bukti')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('bukti')->storeAs('uploadinfaq', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $infaq = Infaq::findOrfail(request('id'));
        $infaq->totalfaq = request('totalfaq');
        $infaq->nama = request('nama');
        $infaq->nohp = request('nohp');
        $infaq->email = request('email');
        if ($request->hasFile('bukti')) {
            $infaq->bukti = $fileNameToStore;
        }
        $infaq->save();

        return redirect()->back()->withSuccess('Data Infaq Berhasil Dirubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idinfaq = base64_decode($id);
        $infaq = Infaq::findOrfail($idinfaq);
        $infaq->delete();
    }
}
