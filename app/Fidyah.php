<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fidyah extends Model
{
    protected $fillable = [
        'jumlahhari','totalfi','nama','nohp','email','bukti','keterangan'
    ];

    public function fidyah()
    {
        return $this->hasMany(Fidyah::class, 'id');
    }

    public static function getmanage()
    {
        $query = Fidyah::all();
        return $query;
    }

    public static function updateketerangan()
    {
        $query = Fidyah::find($id)->where('id', $id)->update(['keterangan'=>'Telah Dikonfirmasi']);
        return $query;

    }
}
