<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penghasilan extends Model
{
    protected $fillable = [
        'harga_beras','gaji_bulan','jumlah_zakat','nama','nohp','email',
    ];


    public function penghasilan()
    {
        return $this->hasMany(Penghasilan::class, 'id');
    }

    public static function getmanage()
    {
        $query = Penghasilan::all();
        return $query;
    }
}
