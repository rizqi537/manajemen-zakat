<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','description','photo' 
    ];

    public function artikel()
    {
        return $this->hasMany(Artikel::class, 'id');
    }

    public static function getmanage()
    {
        $query = Artikel::all();
        return $query;
    }

    public static function getartikel()
    {
        $query = Artikel::orderby('created_at','desc')->paginate(8);
        return $query;
    }

}
