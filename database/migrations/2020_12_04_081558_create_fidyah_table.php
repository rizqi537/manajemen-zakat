<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFidyahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fidyahs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jumlahhari');
            $table->integer('totalfi');
            $table->string('nama')->nullable();
            $table->string('nohp')->nullable();
            $table->string('email')->nullable();
            $table->string('bukti')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fidyahs');
    }
}
