<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFitrahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fitrahs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('harga_beras');
            $table->integer('jumlah_jiwa');
            $table->integer('totalfit');
            $table->string('nama')->nullable();
            $table->string('nohp')->nullable();
            $table->string('email')->nullable();
            $table->string('bukti')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fitrahs');
    }
}
