<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultipleToPengeluaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengeluarans', function (Blueprint $table) {
            $table->string('nama')->nullable()->after('bukti');
            $table->string('alamat')->nullable()->after('bukti');
            $table->string('nik')->nullable()->after('bukti');
            $table->string('pekerjaan')->nullable()->after('bukti');
            $table->string('status')->nullable()->after('bukti');
            $table->integer('jumlah_mustahik')->nullable()->after('bukti');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengeluarans', function (Blueprint $table) {
            //
        });
    }
}
