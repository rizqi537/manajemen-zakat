<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengeluaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengeluarans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('typezakat_id')->unsigned();
            $table->integer('typemustahiq_id')->unsigned();
            $table->integer('jumlah')->nullable();
            $table->string('keterangan')->nullable();
            $table->integer('user_id')->unsigned();
            $table->index('user_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('typezakat_id')->references('id')->on('typezakats')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('typemustahiq_id')->references('id')->on('jenis_mustahiqs')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengeluarans');
    }
}
