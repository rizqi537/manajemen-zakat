<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infaqs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('totalfaq');
            $table->string('nama')->nullable();
            $table->string('nohp')->nullable();
            $table->string('email')->nullable();
            $table->string('bukti')->nullable();
            $table->string('keterangan')->nullable();
            $table->integer('tipe_zakat')->default(4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infaqs');
    }
}
