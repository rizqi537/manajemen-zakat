-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Feb 2021 pada 03.58
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zakat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikels`
--

CREATE TABLE `artikels` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `artikels`
--

INSERT INTO `artikels` (`id`, `title`, `description`, `photo`, `created_at`, `updated_at`) VALUES
(5, 'Test 1', 'test1', 'Forza3_1609227787.jpg', '2020-12-29 07:43:07', '2020-12-29 07:43:07'),
(6, 'test2', 'test2', 'Forza3_1609300389.jpg', '2020-12-30 03:53:09', '2020-12-30 03:53:09'),
(7, 'test3', 'test3', '953682_1609300439.jpg', '2020-12-30 03:53:59', '2020-12-30 03:53:59'),
(9, 'test5', 'test5', '953682_1609316713.jpg', '2020-12-30 08:25:13', '2020-12-30 08:25:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `beras`
--

CREATE TABLE `beras` (
  `id` int(10) UNSIGNED NOT NULL,
  `uang_beras` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `beras`
--

INSERT INTO `beras` (`id`, `uang_beras`, `created_at`, `updated_at`) VALUES
(12, 10000, '2020-12-28 09:16:48', '2020-12-28 09:16:48'),
(13, 11000, '2020-12-28 08:36:52', '2020-12-28 08:36:52'),
(14, 11500, '2020-12-28 08:37:02', '2020-12-28 08:37:02'),
(15, 12000, '2020-12-28 08:37:12', '2020-12-28 08:37:12'),
(16, 12500, '2020-12-28 08:37:24', '2020-12-28 08:37:24'),
(17, 13000, '2020-12-28 08:37:31', '2020-12-28 08:37:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fidyahs`
--

CREATE TABLE `fidyahs` (
  `id` int(10) UNSIGNED NOT NULL,
  `jumlahhari` int(11) NOT NULL,
  `totalfi` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT 'Belum Dikonfirmasi',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipe_zakat` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `fidyahs`
--

INSERT INTO `fidyahs` (`id`, `jumlahhari`, `totalfi`, `nama`, `nohp`, `email`, `bukti`, `keterangan`, `created_at`, `updated_at`, `tipe_zakat`) VALUES
(5, 15, 675000, 'rizqi', '085721221226', 'rizqi537@gmail.com', 'full-hd-sunset-wallpaper-3_1613006949.jpg', 'Belum Dikonfirmasi', '2021-01-08 00:30:09', '2021-02-11 01:29:09', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fitrahs`
--

CREATE TABLE `fitrahs` (
  `id` int(10) UNSIGNED NOT NULL,
  `harga_beras` int(11) NOT NULL,
  `jumlah_jiwa` int(11) NOT NULL,
  `totalfit` int(11) NOT NULL,
  `jumlah_infaq` int(11) DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Dikonfirmasi',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipe_zakat` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `fitrahs`
--

INSERT INTO `fitrahs` (`id`, `harga_beras`, `jumlah_jiwa`, `totalfit`, `jumlah_infaq`, `nama`, `nohp`, `email`, `bukti`, `keterangan`, `created_at`, `updated_at`, `tipe_zakat`) VALUES
(17, 11500, 6, 172500, NULL, 'rizqi', '085721221226', 'rizqiaf@bprkaryagunamandiri.co.id', '953682_1613003813.jpg', 'Telah Dikonfirmasi', '2021-01-27 23:59:07', '2021-02-11 00:36:53', 1),
(18, 12500, 9, 281250, NULL, 'rizqi', '085912312412', 'rizqi537@gmail.com', '953682_1613004100.jpg', 'Belum Dikonfirmasi', '2021-02-10 04:39:02', '2021-02-11 00:41:40', 1),
(19, 11500, 10, 287500, NULL, 'rizqi', '085721234512', 'rizqiaf@bprkaryagunamandiri.co.id', NULL, 'Belum Dikonfirmasi', '2021-02-11 02:47:23', '2021-02-11 02:47:23', 1),
(20, 13000, 10, 325000, 250000, 'rizqi', '089123418123', 'rizqiaf@bprkaryagunamandiri.co.id', NULL, 'Belum Dikonfirmasi', '2021-02-11 02:48:58', '2021-02-11 02:48:58', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_masuks`
--

CREATE TABLE `history_masuks` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `OS` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `history_masuks`
--

INSERT INTO `history_masuks` (`id`, `ip_address`, `OS`, `browser`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '::1', 'Windows 10', 'Mozilla Firefox v.83.0', 2, '2020-11-21 15:18:49', '2020-11-21 15:18:49'),
(2, '::1', 'Windows 10', 'Mozilla Firefox v.83.0', 2, '2020-11-22 06:04:11', '2020-11-22 06:04:11'),
(3, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.66', 2, '2020-11-23 02:56:57', '2020-11-23 02:56:57'),
(4, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.66', 2, '2020-11-23 06:53:23', '2020-11-23 06:53:23'),
(5, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.66', 2, '2020-11-23 08:40:21', '2020-11-23 08:40:21'),
(6, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.66', 2, '2020-11-23 08:47:17', '2020-11-23 08:47:17'),
(7, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.66', 3, '2020-11-23 08:47:40', '2020-11-23 08:47:40'),
(8, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.66', 3, '2020-11-24 01:37:00', '2020-11-24 01:37:00'),
(9, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-11-24 04:28:43', '2020-11-24 04:28:43'),
(10, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 2, '2020-11-24 04:30:25', '2020-11-24 04:30:25'),
(11, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-11-24 04:30:52', '2020-11-24 04:30:52'),
(12, '::1', 'Windows 10', 'Mozilla Firefox v.83.0', 3, '2020-11-27 10:26:17', '2020-11-27 10:26:17'),
(13, '::1', 'Windows 10', 'Google Chrome v.86.0.4240.198', 3, '2020-11-27 10:28:21', '2020-11-27 10:28:21'),
(14, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-11-30 00:22:56', '2020-11-30 00:22:56'),
(15, '::1', 'Windows 10', 'Google Chrome v.86.0.4240.198', 3, '2020-12-01 00:06:06', '2020-12-01 00:06:06'),
(16, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-01 00:28:32', '2020-12-01 00:28:32'),
(17, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-01 04:05:51', '2020-12-01 04:05:51'),
(18, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-02 09:33:27', '2020-12-02 09:33:27'),
(19, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-02 09:35:14', '2020-12-02 09:35:14'),
(20, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-03 02:45:13', '2020-12-03 02:45:13'),
(21, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-03 07:37:10', '2020-12-03 07:37:10'),
(22, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-03 09:37:29', '2020-12-03 09:37:29'),
(23, '::1', 'Windows 10', 'Mozilla Firefox v.83.0', 3, '2020-12-04 00:19:01', '2020-12-04 00:19:01'),
(24, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-04 06:32:40', '2020-12-04 06:32:40'),
(25, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-04 09:32:15', '2020-12-04 09:32:15'),
(26, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-07 04:06:26', '2020-12-07 04:06:26'),
(27, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.67', 3, '2020-12-08 00:17:49', '2020-12-08 00:17:49'),
(28, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-08 06:15:03', '2020-12-08 06:15:03'),
(29, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-08 08:40:57', '2020-12-08 08:40:57'),
(30, '::1', 'Windows 10', 'Mozilla Firefox v.83.0', 3, '2020-12-08 10:29:23', '2020-12-08 10:29:23'),
(31, '::1', 'Windows 10', 'Mozilla Firefox v.83.0', 3, '2020-12-10 00:57:23', '2020-12-10 00:57:23'),
(32, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-10 09:32:52', '2020-12-10 09:32:52'),
(33, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-10 09:52:02', '2020-12-10 09:52:02'),
(34, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-10 10:03:14', '2020-12-10 10:03:14'),
(35, '::1', 'Windows 10', 'Mozilla Firefox v.83.0', 3, '2020-12-11 00:04:49', '2020-12-11 00:04:49'),
(36, '::1', 'Windows 10', 'Mozilla Firefox v.83.0', 3, '2020-12-11 00:05:58', '2020-12-11 00:05:58'),
(37, '::1', 'Windows 10', 'Mozilla Firefox v.83.0', 3, '2020-12-11 00:08:09', '2020-12-11 00:08:09'),
(38, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-11 00:38:22', '2020-12-11 00:38:22'),
(39, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-11 00:52:03', '2020-12-11 00:52:03'),
(40, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-11 03:11:19', '2020-12-11 03:11:19'),
(41, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-11 08:49:07', '2020-12-11 08:49:07'),
(42, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-11 09:56:12', '2020-12-11 09:56:12'),
(43, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 4, '2020-12-11 09:57:02', '2020-12-11 09:57:02'),
(44, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-11 09:58:23', '2020-12-11 09:58:23'),
(45, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 4, '2020-12-11 09:59:41', '2020-12-11 09:59:41'),
(46, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 4, '2020-12-14 02:20:57', '2020-12-14 02:20:57'),
(47, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-16 05:11:41', '2020-12-16 05:11:41'),
(48, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-16 06:52:34', '2020-12-16 06:52:34'),
(49, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-17 04:03:39', '2020-12-17 04:03:39'),
(50, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-17 06:08:56', '2020-12-17 06:08:56'),
(51, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-17 06:30:37', '2020-12-17 06:30:37'),
(52, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-17 08:58:52', '2020-12-17 08:58:52'),
(53, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-23 00:10:08', '2020-12-23 00:10:08'),
(54, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-23 06:41:41', '2020-12-23 06:41:41'),
(55, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-23 08:01:20', '2020-12-23 08:01:20'),
(56, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-23 08:35:44', '2020-12-23 08:35:44'),
(57, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-24 02:40:34', '2020-12-24 02:40:34'),
(58, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-24 03:24:09', '2020-12-24 03:24:09'),
(59, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-24 15:30:38', '2020-12-24 15:30:38'),
(60, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-25 03:54:20', '2020-12-25 03:54:20'),
(61, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-25 07:02:16', '2020-12-25 07:02:16'),
(62, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-25 07:30:13', '2020-12-25 07:30:13'),
(63, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-25 07:43:31', '2020-12-25 07:43:31'),
(64, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-25 10:20:22', '2020-12-25 10:20:22'),
(65, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-25 15:45:44', '2020-12-25 15:45:44'),
(66, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-25 16:07:00', '2020-12-25 16:07:00'),
(67, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-25 16:40:24', '2020-12-25 16:40:24'),
(68, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-26 00:10:46', '2020-12-26 00:10:46'),
(69, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-26 00:12:35', '2020-12-26 00:12:35'),
(70, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-26 00:13:50', '2020-12-26 00:13:50'),
(71, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-26 00:20:09', '2020-12-26 00:20:09'),
(72, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-26 00:24:03', '2020-12-26 00:24:03'),
(73, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-26 00:28:26', '2020-12-26 00:28:26'),
(74, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 00:18:55', '2020-12-28 00:18:55'),
(75, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 00:50:47', '2020-12-28 00:50:47'),
(76, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 00:57:57', '2020-12-28 00:57:57'),
(77, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 02:00:21', '2020-12-28 02:00:21'),
(78, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 02:42:13', '2020-12-28 02:42:13'),
(79, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 03:34:16', '2020-12-28 03:34:16'),
(80, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 03:37:13', '2020-12-28 03:37:13'),
(81, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 03:39:39', '2020-12-28 03:39:39'),
(82, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 03:52:18', '2020-12-28 03:52:18'),
(83, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 07:49:11', '2020-12-28 07:49:11'),
(84, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 09:16:37', '2020-12-28 09:16:37'),
(85, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-28 09:59:09', '2020-12-28 09:59:09'),
(86, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-29 00:34:44', '2020-12-29 00:34:44'),
(87, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-29 02:16:11', '2020-12-29 02:16:11'),
(88, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-29 08:50:40', '2020-12-29 08:50:40'),
(89, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-29 08:55:22', '2020-12-29 08:55:22'),
(90, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-29 09:36:50', '2020-12-29 09:36:50'),
(91, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 4, '2020-12-29 09:40:13', '2020-12-29 09:40:13'),
(92, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-30 02:44:57', '2020-12-30 02:44:57'),
(93, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-30 03:52:54', '2020-12-30 03:52:54'),
(94, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-30 04:31:56', '2020-12-30 04:31:56'),
(95, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-30 04:34:25', '2020-12-30 04:34:25'),
(96, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-30 04:43:10', '2020-12-30 04:43:10'),
(97, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-30 06:10:34', '2020-12-30 06:10:34'),
(98, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-30 07:34:16', '2020-12-30 07:34:16'),
(99, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-30 07:37:44', '2020-12-30 07:37:44'),
(100, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2020-12-30 07:59:55', '2020-12-30 07:59:55'),
(101, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-01 09:48:38', '2021-01-01 09:48:38'),
(102, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-01 09:57:00', '2021-01-01 09:57:00'),
(103, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-02 16:13:17', '2021-01-02 16:13:17'),
(104, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-02 17:30:14', '2021-01-02 17:30:14'),
(105, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-03 00:21:25', '2021-01-03 00:21:25'),
(106, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-03 01:26:30', '2021-01-03 01:26:30'),
(107, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-03 02:06:13', '2021-01-03 02:06:13'),
(108, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-03 06:53:55', '2021-01-03 06:53:55'),
(109, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-05 07:01:23', '2021-01-05 07:01:23'),
(110, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-05 10:06:28', '2021-01-05 10:06:28'),
(111, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-06 03:43:43', '2021-01-06 03:43:43'),
(112, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-07 01:09:32', '2021-01-07 01:09:32'),
(113, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-07 07:41:05', '2021-01-07 07:41:05'),
(114, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-07 08:11:36', '2021-01-07 08:11:36'),
(115, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-07 08:51:52', '2021-01-07 08:51:52'),
(116, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-07 11:54:58', '2021-01-07 11:54:58'),
(117, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-08 00:17:43', '2021-01-08 00:17:43'),
(118, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-08 00:21:54', '2021-01-08 00:21:54'),
(119, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-08 00:30:21', '2021-01-08 00:30:21'),
(120, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-08 01:18:37', '2021-01-08 01:18:37'),
(121, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-08 02:26:54', '2021-01-08 02:26:54'),
(122, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-08 03:44:44', '2021-01-08 03:44:44'),
(123, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-08 06:37:37', '2021-01-08 06:37:37'),
(124, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-08 06:39:36', '2021-01-08 06:39:36'),
(125, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-10 01:16:26', '2021-01-10 01:16:26'),
(126, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-10 03:41:49', '2021-01-10 03:41:49'),
(127, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-10 03:53:05', '2021-01-10 03:53:05'),
(128, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-10 06:09:10', '2021-01-10 06:09:10'),
(129, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-10 07:13:02', '2021-01-10 07:13:02'),
(130, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.88', 3, '2021-01-10 10:56:36', '2021-01-10 10:56:36'),
(131, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-11 00:19:25', '2021-01-11 00:19:25'),
(132, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-11 01:10:38', '2021-01-11 01:10:38'),
(133, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-11 04:00:39', '2021-01-11 04:00:39'),
(134, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-11 04:36:49', '2021-01-11 04:36:49'),
(135, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-11 04:43:44', '2021-01-11 04:43:44'),
(136, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-11 04:45:37', '2021-01-11 04:45:37'),
(137, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-11 07:34:15', '2021-01-11 07:34:15'),
(138, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-11 07:36:09', '2021-01-11 07:36:09'),
(139, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-12 00:14:06', '2021-01-12 00:14:06'),
(140, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-12 04:36:54', '2021-01-12 04:36:54'),
(141, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-12 04:37:38', '2021-01-12 04:37:38'),
(142, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-12 06:28:56', '2021-01-12 06:28:56'),
(143, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-12 07:49:10', '2021-01-12 07:49:10'),
(144, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-12 08:49:13', '2021-01-12 08:49:13'),
(145, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-13 01:46:59', '2021-01-13 01:46:59'),
(146, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-13 07:01:03', '2021-01-13 07:01:03'),
(147, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-18 08:08:19', '2021-01-18 08:08:19'),
(148, '127.0.0.1', 'Windows 10', 'Mozilla Firefox v.84.0', 3, '2021-01-18 22:15:44', '2021-01-18 22:15:44'),
(149, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-19 01:03:01', '2021-01-19 01:03:01'),
(150, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-19 08:08:05', '2021-01-19 08:08:05'),
(151, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-20 09:18:44', '2021-01-20 09:18:44'),
(152, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-20 09:21:16', '2021-01-20 09:21:16'),
(153, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 8, '2021-01-20 09:27:21', '2021-01-20 09:27:21'),
(154, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-20 09:42:41', '2021-01-20 09:42:41'),
(155, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 8, '2021-01-21 01:58:15', '2021-01-21 01:58:15'),
(156, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-21 01:58:47', '2021-01-21 01:58:47'),
(157, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-21 02:00:21', '2021-01-21 02:00:21'),
(158, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-21 05:33:20', '2021-01-21 05:33:20'),
(159, '::1', 'Windows 10', 'Google Chrome v.87.0.4280.141', 3, '2021-01-21 09:39:44', '2021-01-21 09:39:44'),
(160, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.96', 3, '2021-01-22 07:30:53', '2021-01-22 07:30:53'),
(161, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.96', 3, '2021-01-25 02:58:07', '2021-01-25 02:58:07'),
(162, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.96', 8, '2021-01-25 02:59:10', '2021-01-25 02:59:10'),
(163, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.96', 3, '2021-01-25 04:33:15', '2021-01-25 04:33:15'),
(164, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.96', 3, '2021-01-27 23:59:28', '2021-01-27 23:59:28'),
(165, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-09 07:24:32', '2021-02-09 07:24:32'),
(166, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-09 08:20:45', '2021-02-09 08:20:45'),
(167, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-10 00:14:21', '2021-02-10 00:14:21'),
(168, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-10 00:22:33', '2021-02-10 00:22:33'),
(169, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-10 04:41:04', '2021-02-10 04:41:04'),
(170, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-10 05:38:53', '2021-02-10 05:38:53'),
(171, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-11 00:37:51', '2021-02-11 00:37:51'),
(172, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-11 00:42:54', '2021-02-11 00:42:54'),
(173, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-11 00:48:24', '2021-02-11 00:48:24'),
(174, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-11 01:35:54', '2021-02-11 01:35:54'),
(175, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-11 01:44:50', '2021-02-11 01:44:50'),
(176, '::1', 'Windows 10', 'Google Chrome v.88.0.4324.150', 3, '2021-02-11 02:35:14', '2021-02-11 02:35:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `infaqs`
--

CREATE TABLE `infaqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `totalfaq` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Belum Dikonfirmasi',
  `tipe_zakat` int(11) NOT NULL DEFAULT '4',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `infaqs`
--

INSERT INTO `infaqs` (`id`, `totalfaq`, `nama`, `nohp`, `email`, `bukti`, `keterangan`, `tipe_zakat`, `created_at`, `updated_at`) VALUES
(5, 300000, 'rizqi', '085721221226', 'rizqiaf@bprkaryagunamandiri.co.id', 'full-hd-sunset-wallpaper-3_1613007940.jpg', 'Belum Dikonfirmasi', 4, '2021-01-11 04:43:26', '2021-02-11 01:45:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_mustahiqs`
--

CREATE TABLE `jenis_mustahiqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_bagian` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jenis_mustahiqs`
--

INSERT INTO `jenis_mustahiqs` (`id`, `jenis`, `keterangan`, `jumlah_bagian`, `created_at`, `updated_at`) VALUES
(1, 'Fakir', 'orang yang tidak memiliki harta', 150000, '2020-11-21 15:10:56', '2020-11-21 15:10:56'),
(2, 'Miskin', 'orang yang penghasilannya tidak mencukupi', 150000, '2020-11-21 15:10:56', '2020-11-21 15:10:56'),
(3, 'Riqab', 'hamba sahaya atau budak', 150000, '2020-11-21 15:10:56', '2020-11-21 15:10:56'),
(4, 'Gharim', 'orang yang memiliki banyak hutang', 150000, '2020-11-21 15:10:56', '2020-11-21 15:10:56'),
(5, 'Mualaf', 'orang yang baru masuk Islam', 150000, '2020-11-21 15:10:56', '2020-11-21 15:10:56'),
(6, 'Fisabilillah', 'pejuang di jalan Allah', 150000, '2020-11-21 15:10:56', '2020-11-21 15:10:56'),
(7, 'Ibnu Sabil', 'musafir dan para pelajar perantauan', 150000, '2020-11-21 15:10:56', '2020-11-21 15:10:56'),
(8, 'Amil Zakat', 'panitia penerima dan pengelola dana zakat', 150000, '2020-11-21 15:10:56', '2020-11-21 15:10:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_zakats`
--

CREATE TABLE `jenis_zakats` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jenis_zakats`
--

INSERT INTO `jenis_zakats` (`id`, `jenis`, `nominal`, `created_at`, `updated_at`) VALUES
(1, 0, 0, NULL, NULL),
(2, 2, 0, NULL, NULL),
(3, 0, 0, NULL, NULL),
(4, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_02_120410_create_roles_table', 1),
(4, '2018_05_02_123319_create_jenis_mustahiqs_table', 1),
(5, '2018_05_02_132036_create_mustahiqs_table', 1),
(6, '2018_05_02_132556_create_muzakkis_table', 1),
(7, '2018_05_02_135205_create_jenis_zakats_table', 1),
(8, '2018_05_02_135350_create_pengeluarans_table', 1),
(9, '2018_05_02_141207_create_transaksis_table', 1),
(10, '2018_05_11_141504_create_history_masuks_table', 1),
(11, '2020_11_30_081817_create_beras_table', 2),
(12, '2020_12_04_081558_create_fidyah_table', 3),
(13, '2020_12_14_090944_create_fitrahs_table', 4),
(14, '2020_12_29_141011_create_artikels_table', 5),
(15, '2021_01_02_231927_create_penghasilans_table', 6),
(16, '2021_01_02_233211_create_penghasilans_table', 7),
(17, '2021_01_05_134738_create_typezakats_table', 8),
(18, '2021_01_07_184022_add_column_type_zakat_fitrah_table', 9),
(19, '2021_01_07_184117_add_column_type_zakat_fidyah_table', 9),
(20, '2021_01_07_184146_add_column_type_zakat_income_table', 9),
(21, '2021_01_08_092529_create_infaqs_table', 10),
(22, '2021_01_10_131031_create_pengeluarans_table', 11),
(23, '2021_01_10_162805_create_pengeluarans_table', 12),
(24, '2021_01_11_132832_add_bukti_to_pengeluarans', 13),
(25, '2021_02_10_114638_add_nama_to_pengeluarans_table', 14),
(26, '2021_02_10_120440_add_multiple_to_pengeluarans_table', 15),
(27, '2021_02_11_092503_add_infaq_to_fitrah_table', 16);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mustahiqs`
--

CREATE TABLE `mustahiqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenismustahiq_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `mustahiqs`
--

INSERT INTO `mustahiqs` (`id`, `name`, `area`, `jenismustahiq_id`, `created_at`, `updated_at`) VALUES
(2, 'Wendi', 'Bandung', 8, '2020-11-24 06:36:41', '2020-11-24 06:36:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `muzakkis`
--

CREATE TABLE `muzakkis` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jeniskelamin` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `muzakkis`
--

INSERT INTO `muzakkis` (`id`, `name`, `email`, `nohp`, `alamat`, `jeniskelamin`, `created_at`, `updated_at`) VALUES
(1, 'Wembley', 'rizqi537@gmail.com', '92132184123', 'aadasdwad', 'L', '2020-11-22 06:05:58', '2020-11-22 06:08:00'),
(4, 'Dini', 'dinipratiwig@gmail.com', '0213921304', 'asdawd', 'P', '2020-11-22 06:11:27', '2020-11-22 06:11:27'),
(5, 'Eva', 'evanuralam28@gmail.com', '02132138214', 'Dago', 'P', '2020-11-24 01:53:21', '2020-11-24 01:53:21'),
(6, 'Eva', 'evanuralam28@gmail.com', '02132138214', 'Dago', 'P', '2020-11-24 01:53:45', '2020-11-24 01:53:45'),
(8, 'Rizqi', 'rizqi537@gmail.com', '324123', 'asdawdasd', 'L', '2020-11-24 06:41:09', '2020-11-24 06:41:09'),
(9, 'Rizqi', 'adawd', '213124', 'asdaawd', 'L', '2020-11-24 06:52:31', '2020-11-24 06:52:31'),
(10, 'Jaka', 'rizqidarlyn@gmail.com', '21312412312', 'rasdadawd', 'L', '2020-11-27 10:30:19', '2020-11-27 10:30:19'),
(11, 'Dini Pratiwi Gunadi', 'rizqi537@gmail.com', '085721221226', 'sdawdadawdsd', NULL, '2020-11-30 01:35:54', '2020-11-30 01:35:54'),
(12, 'Dini Pratiwi Gunadi', NULL, '085721221226', 'sdawdadawdsd', NULL, '2020-11-30 01:36:11', '2020-11-30 01:36:11'),
(13, 'rizqi', 'rizqi537@gmail.com', '085721221226', 'Jl. Babakan Tarogong', 'L', '2020-12-23 06:46:31', '2020-12-23 06:46:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengeluarans`
--

CREATE TABLE `pengeluarans` (
  `id` int(10) UNSIGNED NOT NULL,
  `typezakat_id` int(10) UNSIGNED NOT NULL,
  `typemustahiq_id` int(10) UNSIGNED NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `bukti` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nik` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pekerjaan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_mustahik` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pengeluarans`
--

INSERT INTO `pengeluarans` (`id`, `typezakat_id`, `typemustahiq_id`, `jumlah`, `keterangan`, `user_id`, `bukti`, `nama`, `alamat`, `nik`, `pekerjaan`, `status`, `jumlah_mustahik`, `created_at`, `updated_at`) VALUES
(13, 1, 4, 200000, 'Penyaluran 4', 3, '953682_1610348193.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-11 06:56:33', '2021-01-11 06:56:33'),
(14, 1, 5, 100000, 'Penyaluran 4', 3, '953682_1610350635.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-11 07:37:15', '2021-01-11 07:37:15'),
(15, 2, 4, 200000, 'Penyaluran 5', 3, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-13 04:21:55', '2021-01-13 04:21:55'),
(16, 3, 7, 100000, 'Penyaluran 6', 3, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-13 09:18:01', '2021-01-13 09:18:01'),
(17, 1, 1, 200000, 'Penyaluran 8', 3, '953682_1612856204.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-09 07:36:44', '2021-02-09 07:36:44'),
(18, 1, 1, 200000, 'Penyaluran 10', 3, '953682_1612856451.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-09 07:40:51', '2021-02-09 07:40:51'),
(20, 1, 4, 500000, 'Penyaluran 12', 3, '953682_1612859040.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-09 08:24:01', '2021-02-09 08:24:01'),
(21, 1, 1, 600000, 'Penyaluran 14', 3, '953682_1612860868.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-09 08:54:28', '2021-02-09 08:54:28'),
(22, 3, 5, 600000, 'Penyaluran 15', 3, '953682_1612860890.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-09 08:54:50', '2021-02-09 08:54:50'),
(23, 4, 4, 100000, 'Penyaluran 15', 3, '953682_1612935696.jpg', 'jaka', 'cimahi', '33142123412', 'karyawan swasta', 'tetap', 5, '2021-02-10 05:41:36', '2021-02-10 05:41:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penghasilans`
--

CREATE TABLE `penghasilans` (
  `id` int(10) UNSIGNED NOT NULL,
  `harga_beras` int(11) NOT NULL,
  `gaji_bulan` int(11) NOT NULL,
  `jumlah_zakat` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Dikonfirmasi',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipe_zakat` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `penghasilans`
--

INSERT INTO `penghasilans` (`id`, `harga_beras`, `gaji_bulan`, `jumlah_zakat`, `nama`, `nohp`, `email`, `bukti`, `keterangan`, `created_at`, `updated_at`, `tipe_zakat`) VALUES
(6, 12000, 7000000, 175000, 'rizqi', '085721221226', 'rizqi537@gmail.com', 'full-hd-sunset-wallpaper-3_1613008954.jpg', 'Telah Dikonfirmasi', '2021-01-08 00:21:02', '2021-02-11 02:02:34', 3),
(7, 11500, 6400000, 160000, 'rizqi', '085721221226', 'rizqiaf@bprkaryagunamandiri.co.id', '953682_1610425341.jpg', 'Telah Dikonfirmasi', '2021-01-12 04:22:07', '2021-01-12 04:22:48', 3),
(8, 11000, 6000000, 150000, 'rizqi', '085721221226', 'rizqi537@gmail.com', NULL, 'Telah Dikonfirmasi', '2021-01-25 09:30:36', '2021-01-28 00:01:28', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'Pengelola Website atau Webmaster', '2020-11-21 15:17:41', '2020-11-21 15:17:41'),
(2, 'Petugas', 'Petugas pengelola zakat', '2020-11-21 15:17:41', '2020-11-21 15:17:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksis`
--

CREATE TABLE `transaksis` (
  `id` int(10) UNSIGNED NOT NULL,
  `muzakki_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `jeniszakat_id` int(10) UNSIGNED NOT NULL,
  `jiwa` int(11) NOT NULL,
  `beras_fitrah` double(5,1) DEFAULT NULL,
  `uang_fitrah` int(11) DEFAULT NULL,
  `fidyah` int(11) DEFAULT NULL,
  `zakat_maal` int(11) DEFAULT NULL,
  `infaq` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transaksis`
--

INSERT INTO `transaksis` (`id`, `muzakki_id`, `user_id`, `jeniszakat_id`, `jiwa`, `beras_fitrah`, `uang_fitrah`, `fidyah`, `zakat_maal`, `infaq`, `deleted_at`, `created_at`, `updated_at`) VALUES
(9, 5, 3, 2, 2, 7.0, 600000, 600000, 600000, 600000, '2020-11-24 06:27:25', '2020-11-24 06:27:09', '2020-11-24 06:27:25'),
(10, 5, 3, 3, 1, 3.5, 400000, 300000, 300000, 300000, NULL, '2020-11-24 06:32:23', '2020-11-24 06:46:59'),
(11, 8, 3, 1, 2, 7.0, 500000, 500000, 500000, 500000, NULL, '2020-11-24 06:41:09', '2020-11-24 06:41:09'),
(12, 10, 3, 2, 3, 10.5, 500000, 500000, 500000, 500000, NULL, '2020-11-27 10:30:19', '2020-11-27 10:30:19'),
(13, 12, 3, 2, 1, 3.5, 0, NULL, 500000, 500000, NULL, '2020-11-30 01:36:11', '2020-11-30 01:36:11'),
(14, 4, 3, 4, 5, 17.5, 500000, 500000, 500000, 500000, NULL, '2020-12-04 06:34:35', '2020-12-04 06:34:35'),
(15, 13, 3, 4, 4, 14.0, 500000, 500000, 500000, 500000, NULL, '2020-12-23 06:46:31', '2020-12-23 06:46:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `typezakats`
--

CREATE TABLE `typezakats` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_zakat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_zakat` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `typezakats`
--

INSERT INTO `typezakats` (`id`, `nama_zakat`, `total_zakat`, `created_at`, `updated_at`) VALUES
(1, 'Zakat Fitrah', 100000, '2021-01-05 09:41:47', '2021-02-09 08:54:28'),
(2, 'Zakat Fidyah', 300000, '2021-01-06 06:14:38', '2021-01-13 04:21:55'),
(3, 'Zakat Penghasilan', 390000, '2021-01-06 06:14:52', '2021-02-09 08:54:50'),
(4, 'Zakat Infaq', 1100000, '2021-01-06 06:15:22', '2021-02-10 05:41:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT '2',
  `status` enum('aktif','tidak aktif') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tidak aktif',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `role_id`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Webmaster', 'admin01', 'nurmuhlis74@yahoo.co.id', '$2y$10$rRVV9QsVxnAgIfj.VG3PYeZH0WlN2ZUvVd4QhLNRtizSVZIvX2ctW', 1, 'aktif', 'qGDgbBZW0gA5wzpZqzcQhNqbYyiUjmAAPodKIU9dM8hN7oeTOMSLFI8HSmSf', '2020-11-21 15:17:51', '2020-11-21 15:17:51'),
(3, 'Rizqi Ahmad', 'rizqi537', 'rizqi537@gmail.com', '$2y$10$pnq5tfPG3gCEFHgFgEyrgenmdobRmNyNWjODtcVWcjv0gEkjRbUOC', 1, 'aktif', 'Z6xE6yhthixBlJlcsRM1YuVctIQM0gSoUQzHV0JGdDJF5IcD4FUbB0UWaU37', '2020-11-23 08:46:52', '2020-12-23 00:19:39'),
(4, 'Jaka', 'jaka12345', 'jaka12345@gmail.com', '$2y$10$BOeyBEC6rHmhS7.IEsSZ1upRCJ/6tvQqXc9Ugghpy6hsjEqj51Jxq', 2, 'aktif', 'D63AEoLHOqz6QRsV0VnuJP14d93DjuS2DnFWUQBmlmyoe8aRhcO5vRy6lOze', '2020-12-11 09:55:59', '2020-12-29 09:38:34'),
(7, 'Dini Pratiwi Gunadi', 'dini77', 'dinipratiwig@gmail.com', '$2y$10$LBsJQU4Nd/hiKGHeKkIGuuaMjwqx7nNehToCX1xzzE92t9bEasGUu', 2, 'tidak aktif', 'exawxkWgzJZCbRiMNVflzzWycQHO6qJJo5r7h8cVzB9nGIocqru0DCS1vF9m', '2021-01-10 03:43:35', '2021-01-10 03:43:35'),
(8, 'Vani Aspani', 'vani22', 'vanimaret22@gmail.com', '$2y$10$.GPYjoHato6C9xxtc.zidOa.3IwPT3tOX73CuVfyfLBLYb.UfIcT2', 2, 'aktif', 'kWnbm9lmydUtnPqCuNG7V589vLgmah0yoManyT9tR0ANtnrDyp1YZgJGJ3An', '2021-01-20 09:21:01', '2021-01-20 09:21:30'),
(15, 'Enjang Diana', 'enjang12345', 'umum@bprkaryagunamandiri.co.id', '$2y$10$75dsItcOgR4TFjykYb219ubZRdUyt552c53gcn7AKTR.LV4nRLThe', 2, 'tidak aktif', 'huOoNXF1frjb2wjKi1q1gRjJ1IrVDjGDDl9KNNYxYdniHgxO9NnHnfzzUS0b', '2021-01-25 04:38:50', '2021-01-25 04:38:50');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `artikels`
--
ALTER TABLE `artikels`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `beras`
--
ALTER TABLE `beras`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `fidyahs`
--
ALTER TABLE `fidyahs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `fitrahs`
--
ALTER TABLE `fitrahs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `history_masuks`
--
ALTER TABLE `history_masuks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_masuks_user_id_index` (`user_id`);

--
-- Indeks untuk tabel `infaqs`
--
ALTER TABLE `infaqs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jenis_mustahiqs`
--
ALTER TABLE `jenis_mustahiqs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jenis_zakats`
--
ALTER TABLE `jenis_zakats`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mustahiqs`
--
ALTER TABLE `mustahiqs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mustahiqs_jenismustahiq_id_index` (`jenismustahiq_id`);

--
-- Indeks untuk tabel `muzakkis`
--
ALTER TABLE `muzakkis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pengeluarans`
--
ALTER TABLE `pengeluarans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengeluarans_user_id_index` (`user_id`),
  ADD KEY `pengeluarans_typezakat_id_foreign` (`typezakat_id`),
  ADD KEY `pengeluarans_typemustahiq_id_foreign` (`typemustahiq_id`);

--
-- Indeks untuk tabel `penghasilans`
--
ALTER TABLE `penghasilans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksis`
--
ALTER TABLE `transaksis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksis_muzakki_id_user_id_jeniszakat_id_index` (`muzakki_id`,`user_id`,`jeniszakat_id`),
  ADD KEY `transaksis_user_id_foreign` (`user_id`),
  ADD KEY `transaksis_jeniszakat_id_foreign` (`jeniszakat_id`);

--
-- Indeks untuk tabel `typezakats`
--
ALTER TABLE `typezakats`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `artikels`
--
ALTER TABLE `artikels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `beras`
--
ALTER TABLE `beras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `fidyahs`
--
ALTER TABLE `fidyahs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `fitrahs`
--
ALTER TABLE `fitrahs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `history_masuks`
--
ALTER TABLE `history_masuks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT untuk tabel `infaqs`
--
ALTER TABLE `infaqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `jenis_mustahiqs`
--
ALTER TABLE `jenis_mustahiqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `jenis_zakats`
--
ALTER TABLE `jenis_zakats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `mustahiqs`
--
ALTER TABLE `mustahiqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `muzakkis`
--
ALTER TABLE `muzakkis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `pengeluarans`
--
ALTER TABLE `pengeluarans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `penghasilans`
--
ALTER TABLE `penghasilans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `transaksis`
--
ALTER TABLE `transaksis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `typezakats`
--
ALTER TABLE `typezakats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `history_masuks`
--
ALTER TABLE `history_masuks`
  ADD CONSTRAINT `history_masuks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `mustahiqs`
--
ALTER TABLE `mustahiqs`
  ADD CONSTRAINT `mustahiqs_jenismustahiq_id_foreign` FOREIGN KEY (`jenismustahiq_id`) REFERENCES `jenis_mustahiqs` (`id`);

--
-- Ketidakleluasaan untuk tabel `pengeluarans`
--
ALTER TABLE `pengeluarans`
  ADD CONSTRAINT `pengeluarans_typemustahiq_id_foreign` FOREIGN KEY (`typemustahiq_id`) REFERENCES `jenis_mustahiqs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengeluarans_typezakat_id_foreign` FOREIGN KEY (`typezakat_id`) REFERENCES `typezakats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengeluarans_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaksis`
--
ALTER TABLE `transaksis`
  ADD CONSTRAINT `transaksis_jeniszakat_id_foreign` FOREIGN KEY (`jeniszakat_id`) REFERENCES `jenis_zakats` (`id`),
  ADD CONSTRAINT `transaksis_muzakki_id_foreign` FOREIGN KEY (`muzakki_id`) REFERENCES `muzakkis` (`id`),
  ADD CONSTRAINT `transaksis_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
