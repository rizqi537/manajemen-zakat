<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return redirect('/home');
});*/

Route::get('/', 'FrontController@firstpage');
Route::get('/findzakatberas', 'FrontController@beras');
Route::get('/about','FrontController@about');
Route::get('/kalkulatorzakat','FrontController@kalkulator');


// Zakat Fidiah Frontend
Route::post('tambah-fidyah','FidyahController@store')->name('fidyah.store');
Route::get('/zakatfidyah', 'FrontController@fidyah');
Route::get('/buktifidiah', 'FrontController@buktifidiah');
Route::get('/fidiah/cari', 'FidyahController@show_insert')->name('fidiah.inputkonfirmasi');
Route::get('invoice-fidyah/{id}','FidyahController@createPDF')->name('fidyah.invoice');

// Zakat Fitrah Frontened
Route::get('/zakatfitrah', 'FrontController@zakatfitrahpage');
Route::get('/buktifitrah', 'FrontController@buktifitrah');
Route::get('/fitrah/cari', 'FitrahController@show_insert')->name('fitrah.inputkonfirmasi');
Route::post('/tambah-fitrah','FitrahController@store')->name('fitrah.store');
Route::get('invoice-fitrah/{id}','FitrahController@createPDF')->name('fitrah.invoice');

// Zakat Penghasilan Frontened
Route::get('/form-penghasilan', 'FrontController@penghasilan');
Route::get('/buktipenghasilan', 'FrontController@buktipenghasilan');
Route::get('/penghasilan/cari', 'PenghasilanController@show_insert')->name('penghasilan.inputkonfirmasi');
Route::post('/tambah-penghasilan', 'PenghasilanController@store')->name('penghasilan.store');


// Zakat Infaq Frontend
Route::get('/zakatinfaq', 'FrontController@infaq')->name('infaq');
Route::post('/tambah-infaq','InfaqController@store')->name('infaq.store');
Route::get('/buktiinfaq', 'FrontController@buktiinfaq');
Route::get('/infaq/cari', 'InfaqController@show_insert')->name('infaq.inputkonfirmasi');

Route::get('view-artikel/{id}', 'ArtikelController@view')->name('artikel.view');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/board2','ChartController@getdataboard')->name('board2');
//Route::get('/home','FidyahController@index')->name('fidyah');
Route::get('/zakat-list', 'ZakatController@index')->name('zakat');
Route::get('/pelaksanaan-zakat', 'ZakatController@create')->name('zakat.createOther');
Route::get('/pelaksanaan-zakat/{id}', 'ZakatController@create')->name('zakat.create');
Route::get('nominal/{nominal}', 'ZakatController@getNominal');
Route::get('search/muzakki/{nama}', 'ZakatController@cariMuzakki');
Route::post('bayar-zakat', 'ZakatController@storeZakat')->name('zakat.store');
Route::get('konfirmasi/{id}', 'ZakatController@showInsertedZakat')->name('zakat.confirmation');
Route::get('list-transaksi/', 'ZakatController@getZakatData');
Route::get('edit-transaksi/{id}', 'ZakatController@editZakat')->name('zakat.edit');
Route::patch('update-transaksi/{transaksi}', 'ZakatController@updateZakat')->name('zakat.update');
Route::get('make-invoice/{id}', 'ZakatController@createPDF')->name('zakat.invoice');
Route::delete('zakat/delete/{id}','ZakatController@destroy')->name('zakat.destroy');

Route::get('list-pengguna/', 'UserController@getUserData');
Route::get('/users', 'UserController@index')->name('user');
Route::patch('aktivasi/{id}', 'UserController@activateUser')->name('user.activate');
Route::patch('deaktivasi/{id}', 'UserController@deactivateUser')->name('user.deactivate');
Route::get('/profil/edit', function () {
    return view('user.edit-profil');
})->name('profil.edit');
Route::patch('update-profil/{user}', 'UserController@updateProfil')->name('profil.update');
Route::get('/ganti-password', function () {
    return view('user.ganti-password');
})->name('password.change');
Route::patch('ganti-password/{id}', 'UserController@changePassword')->name('password.update');
Route::get('/role/{id}', 'UserController@editRole')->name('role.edit');
Route::patch('role-update/{id}', 'UserController@updateRole')->name('role.update');
Route::get('log-pengguna/', 'UserController@getUserLog');
Route::get('/history-login', function () {
    return view('user.login-logs');
})->name('user.history');

Route::get('/jenis-zakat', 'ZakatController@showJenis')->name('jeniszakat.change');
Route::get('jenis-zakat/{id}', 'ZakatController@getJenis');
Route::post('jenis-zakat/store', 'ZakatController@storeJenis')->name('jeniszakat.store');

Route::get('/mustahiq', 'MustahiqController@index')->name('mustahiq');
Route::get('list-mustahiq/', 'MustahiqController@getMustahiqData');
Route::get('list-jenismustahiq/', 'MustahiqController@getjenismustahiq');
Route::get('/jenis-mustahiq', 'MustahiqController@showJenis')->name('jenismustahiq.change');
Route::get('jenis-mustahiq/{id}', 'MustahiqController@getJenis');
Route::post('jenis-mustahiq/update/{id}', 'MustahiqController@updateJenis')->name('jenismustahiq.update');
Route::post('tambah-mustahiq', 'MustahiqController@storeMustahiq')->name('mustahiq.store');
Route::get('tampilkan-mustahiq/{id}', 'MustahiqController@getMustahiq')->name('mustahiq.show');
Route::patch('mustahiq-update/{id}', 'MustahiqController@updateMustahiq')->name('mustahiq.update');
Route::delete('mustahiq/delete/{id}','MustahiqController@destroy')->name('mustahiq.destroy');

// Laporan Route
Route::get('/laporan2','ReportController@index2')->name('report3');
Route::get('/laporan', 'ReportController@index')->name('report');
Route::post('buat-laporan', 'ReportController@create')->name('report.create');
Route::post('buat-laporan2', 'ReportController@make')->name('report.make');
Route::post('buat-laporan3', 'ReportController@make2')->name('report.make2');
Route::get('general-report', 'ReportController@show')->name('generalreport');

Route::get('/pengeluaran', 'PengeluaranController@index')->name('pengeluaran');
Route::get('list-pengeluaran/', 'PengeluaranController@show');
Route::post('tambah-pengeluaran', 'PengeluaranController@store')->name('pengeluaran.store');
Route::get('edit-pengeluaran/{id}', 'PengeluaranController@edit')->name('pengeluaran.edit');
Route::patch('pengeluaran-update/{id}', 'PengeluaranController@update')->name('pengeluaran.update');
Route::delete('pengeluaran/delete/{id}','PengeluaranController@destroy')->name('pengeluaran.destroy');

//Beras
Route::get('/beras','BerasController@index')->name('beras');
Route::post('tambah-beras', 'BerasController@store')->name('beras.store');
Route::get('list-zakatberas/', 'BerasController@getBerasData');
Route::get('tampilkan-beras/{id}', 'BerasController@getberas')->name('beras.show');
Route::patch('beras-update/{id}', 'BerasController@update')->name('beras.update');
Route::delete('beras/delete/{id}','BerasController@destroy')->name('beras.destroy');

//Fidyah
Route::get('/fidyah','FidyahController@index')->name('fidyah');
Route::get('list-fidyah/','FidyahController@getFidyahs');
Route::get('tampilkan-fidyah/{id}','FidyahController@show')->name('fidyah.show');
Route::patch('fidyah-update/{id}', 'FidyahController@update')->name('fidyah.update');
Route::delete('fidyah/delete/{id}','FidyahController@destroy')->name('fidyah.destroy');
Route::get('confirmationfidyah/{id}','FidyahController@showinsertfidyah')->name('fidyah.confirmation');
Route::match(['put','patch'],'upload-bukti/{id}','FidyahController@bukti')->name('fidyah.bukti');

//Fitrah
Route::get('/fitrah','FitrahController@index')->name('fitrah');
Route::get('list-fitrah/','FitrahController@getfitrahs');
Route::patch('fitrah-update/{id}', 'FitrahController@update')->name('fitrah.update');
Route::delete('fitrah/delete/{id}','FitrahController@destroy')->name('fitrah.destroy');
Route::get('edit-fitrah/{id}','FitrahController@show')->name('fitrah.show');
Route::get('confirmationfitrah/{id}','FitrahController@showinsertfitrah')->name('fitrah.confirmation');
Route::patch('upload-bayar-fitrah/{id}','FitrahController@bukti')->name('fitrah.bukti');
//Route::get('/test','FitrahController@test')->name('fitrah.test');

//Artikel
Route::get('/artikel','ArtikelController@index')->name('artikel');
Route::get('list-artikel/', 'ArtikelController@getartikeldata');
Route::get('edit-artikel/{id}','ArtikelController@show')->name('artikel.show');
Route::patch('artikel-update/{id}','ArtikelController@update')->name('artikel.update');
Route::post('/tambah-artikel','ArtikelController@store')->name('artikel.store');
Route::delete('artikel/delete/{id}','ArtikelController@destroy')->name('artikel.destroy');

//Penghasilan
Route::get('/penghasilan','PenghasilanController@index')->name('penghasilan');
Route::get('list-penghasilan/', 'PenghasilanController@getpenghasilans');
Route::get('edit-penghasilan/{id}','PenghasilanController@show')->name('penghasilan.show');
Route::get('confirmationincome/{id}','PenghasilanController@showinsertpenghasilan')->name('penghasilan.confirmation');
Route::match(['put','patch'],'upload-bayar/{id}','PenghasilanController@bukti')->name('penghasilan.bukti');
Route::patch('penghasilan-update/{id}', 'PenghasilanController@update')->name('penghasilan.update');
Route::delete('penghasilan/delete/{id}','PenghasilanController@destroy')->name('penghasilan.destroy');
Route::get('invoice-penghasilan/{id}','PenghasilanController@createPDF')->name('penghasilan.invoice');

//Infaq
Route::get('/infaq','InfaqController@index')->name('infaq');
Route::get('list-infaqs/', 'InfaqController@getinfaqs');
Route::get('confirmationinfaq/{id}','InfaqController@showinsertinfaq')->name('infaq.confirmation');
Route::match(['put','patch'],'upload-infaq/{id}','InfaqController@bukti')->name('infaq.bukti');
Route::get('edit-infaq/{id}','InfaqController@edit')->name('infaq.show');
Route::patch('infaq-update/{id}', 'InfaqController@update')->name('infaq.update');
Route::get('invoice-infaq/{id}','InfaqController@createPDF')->name('infaq.invoice');
Route::delete('infaq/delete/{id}','InfaqController@destroy')->name('infaq.destroy');

//Pemasukan Zakat
Route::get('/typezakat','TypezakatController@index')->name('typezakat');
Route::post('/add-typezakat','TypezakatController@store')->name('typezakat.store');
Route::get('typezakat-list/','TypezakatController@getallzakat');

// Route::get('coba', 'ZakatController@coba');
