<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Laporan Harian {{\UserAgent::tanggalIndo($tanggalReport)}}</title>
    <link rel="stylesheet" href="{{ asset('css/report-template.css') }}" media="all" />
  </head>
  <body onload="window.print();">
  
    <header class="clearfix">
      <div id="logo">
        <img src="logo2.png">
      </div>
      <div id="company">
        <h2 class="name">{{ config('app.name', 'Laravel') }}</h2>
        <div>Jln. Cibaligo, Gg. Cimindi Hilir, Rt.04 Rw.30<br>
            Kel. Cibeureum Kec. Cimahi selatan Kota Cimahi, Kode pos 40535</div>
        <div></div>
        <div><a href="miftahulfalah122@gmail.com">miftahulfalah122@gmail.com</a></div>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <h2 class="name">LAPORAN DANA ZAKAT MASUK</h2>
          <div class="to">DIBUAT OLEH:</div>
          <h2 class="name">{{Auth::user()->name}}</h2>
          {{--  <div class="address">796 Silver Harbour, TX 79273, US</div>  --}}
          <div class="email"><a href="mailto:{{Auth::user()->email}}">{{Auth::user()->email}}</a></div>
        </div>
        <div id="invoice">
          {{--  <h1>LAPORAN KE 3</h1>  --}}
          <div class="date">Tanggal Cetak: {{\UserAgent::tanggalIndo(date('Y-m-d'))}}</div>
          {{--  <div class="date">Due Date: 30/06/2014</div>  --}}
        </div>
      </div>
      <h2 class="name">LAPORAN DANA ZAKAT MASUK</h2>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="desc">JENIS</th>
            <th class="unit">JUMLAH ZAKAT</th>
            <!--<th class="qty">JUMLAH BERAS</th>-->
            {{--  <th class="total">TOTAL</th>  --}}
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="no">01</td>
            <td class="desc"><h3>ZAKAT FITRAH</h3></td>
            <td class="unit">Rp. {{number_format($report1,0,'',',')}}</td>
            
            {{--  <td class="total">$1,200.00</td>  --}}
          </tr>
          <tr>
            <td class="no">02</td>
            <td class="desc"><h3>ZAKAT FIDIAH</h3></td>
            <td class="unit">Rp. {{number_format($report2,0,'',',')}}</td>
            
            {{--  <td class="total">$3,200.00</td>  --}}
          </tr>
          <tr>
            <td class="no">03</td>
            <td class="desc"><h3>ZAKAT PENGHASILAN</h3></td>
            <td class="unit">Rp. {{number_format($report3,0,'',',')}}</td>
            
            {{--  <td class="total">$800.00</td>  --}}
          </tr>
          <tr>
            <td class="no">04</td>
            <td class="desc"><h3>ZAKAT INFAQ</h3></td>
            <td class="unit">Rp. {{number_format($report4,0,'',',')}}</td>
            
            {{--  <td class="total">$800.00</td>  --}}
          </tr>
        </tbody>
        <tfoot>
          {{--  <tr>
            <td colspan="1"></td>
            <td colspan="1"></td>
            <td colspan="1">SUBTOTAL</td>
            <td>$5,200.00</td>
          </tr>  --}}
          <tr>
            <td colspan="1"></td>
            <td colspan="1">TOTAL</td>
            <td colspan="1">Rp. {{number_format($totalreport,0,'',',')}}</td>
            
          </tr>
          <tr>
            <td colspan="1"></td>
            <td colspan="1"></td>
            <td colspan="1"></td>
          </tr>
        </tfoot>
      </table>
      <br><br><br><br>
      <div id="thanks">{{Auth::user()->name}}</div>
      {{--  <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
      </div>  --}}
    </main>
    <footer>
      Laporan dibuat di komputer dan valid walau tanpa tanda tangan.
    </footer>
  </body>
</html>