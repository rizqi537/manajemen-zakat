@extends('layouts.app')

@section('title', 'Data Artikel')
@section('content')
<div class="block-header">
            <h2>DAFTAR ARTIKEL</h2>
        </div>
        <div class="row clearfix">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        		<div class="card">
        			<div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-11">
                                <h2>
                                    DATA ARTIKEL
                                </h2>  
                            </div>
                            <div class="col-xs-12 col-sm-1 align-right">
                                <a title="Data Baru" class="btn bg-indigo btn-circle-lg waves-effect waves-circle waves-float col-xs-1" href="#"  data-toggle="modal" data-target="#insertModal"><i class="material-icons">add_circle_outline</i></a>
                            </div>
                        </div>
        			</div>
                    <div class="body">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="tbartikel">
                                <thead>
                                    <tr>
                                        <th>JUDUL</th>
                                        <th>DESKRIPSI</th>
                                        <th>PHOTO</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
        		</div>
        	</div>
        </div>
        <div class="modal fade" id="insertModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Tambah Data Artikel</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('artikel.store')}}" method="post" id="artikel" enctype="multipart/form-data">
                            @csrf
                            <div class="row clearfix">
                                    <div class="form-group form-float">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="title" id="title" class="form-control" required="">
                                                <label class="form-label">Judul</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="textarea" name="description" id="description" class="form-control" required="">
                                                <label class="form-label">Deskripsi</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <label class="form-label">Foto</label>
                                            <div class="form-line">
                                                <input type="file" id="photo" name="photo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <input type="button" name="insert" id="insert" value="TAMBAH" class="btn btn-primary m-t-15 waves-effect">
                                    </div>
                                </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            jQuery(document).ready(function(){
                var table = $('#tbartikel').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    buttons: [
                        {
                            extend: 'print',
                            footer: false,
                            exportOptions: {
                                columns: [0,1,2]
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: false,
                            exportOptions: {
                                columns: [0,1,2]
                            }
                            
                        },
                        {
                            extend: 'excel',
                            footer: false,
                            exportOptions: {
                                columns: [0,1,2]
                            }
                        } 
                    ],
                    ajax: {
                        url: '{{ url("list-artikel") }}'
                    },
                    columns: [
                    {data: 'title', name: 'title'},
                    {data: 'description', name: 'description'},
                    {data: 'photo', name: 'photo',
                        render: function(data, type, full, meta){
                        return "<img src={{ URL::to('/')}}/uploadphoto/"+ data + " width='60' class='img-thumbnail'/>";
                        },
                    },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                });
                $("#insert").on("click", function(){
                    swal({
                        title: 'Apakah Data Sudah Sesuai?',
                        text: "Anda Dapat Mengedit Kembali",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Saya Yakin',
                        cancelButtonText: 'Tidak, batalkan!',
                        }).then((result) => {
                        if (result.value) {
                            $('#artikel').submit();
                        }
					})
                });
                $("#tbartikel").on("click", "#apus", function(e){
                    var csrf_token = $('meta[name="csrf-token"]').attr("content");
                    var id = $(this).data("value");
                    swal({
                        title: 'Apakah Kamu Yakin Ingin Dihapus?',
                        text: "Data Tidak Bisa Dikembalikan",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Saya Yakin',
                        cancelButtonText: 'Tidak, batalkan!',
                        }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                url: "{{ url('artikel/delete') }}"+ '/' + id,
                                type: "POST",
                                data : {'_method' : 'DELETE', '_token' : csrf_token},
                                success : function(){
                                    table.ajax.reload();
                                }
                            })
                        }
					})
                });  
            });
        </script>
@endsection