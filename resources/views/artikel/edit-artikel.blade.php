@extends('layouts.app')

@section('title', 'Data Artikel')
@section('content')
<div class="block-header">
    <h2>DATA ARTIKEL</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<div class="card">
        	<div class="header">
                <h2>
                    DATA ARTIKEL
                </h2>
            </div>
                    <div class="body">
                    	<form class="form-vertical" id="myform" action="{{route('artikel.update', $artikel)}}" method="POST" enctype="multipart/form-data">
                    	<h2 class="card-inside-title">DATA ARTIKEL</h2>
							@csrf
                    		{{  method_field('PATCH') }}
							<input type="hidden" name="_idm" value="{{base64_encode($artikel->id)}}">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="title" name="title" class="form-control" value="{{$artikel->title}}">
                                    <label class="form-label">Judul</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="textarea" id="description" name="description" class="form-control" value="{{$artikel->description}}">
                                    <label class="form-label">Deskripsi</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label class="form-label">Foto</label>
                                <input type="file" id="photo" name="photo">
                                <div class="form-line">
                                    <img src="{{asset("uploadphoto/$artikel->photo")}}" alt="" title="" width="200" height="200"></a>
                                </div>
                            </div>
                    		<div class="row clearfix">
                                <div class="">
                                <input type="button" name="insert" id="insert" value="Ubah" class="btn btn-primary m-t-15 waves-effect">
                                </div>
                            </div>
                    	</form>
                </div>
        </div>
    </div>
</div>
<script>
    $(document).ready( function () {
        $("#insert").click(function(){
				swal({
					title: 'Apakah Datanya Sudah Sesuai?',
					text: "Pastikan Data Yang Anda Berikan Sesuai",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Sudah Sesuai',
 					cancelButtonText: 'Tidak, batalkan!',
					}).then((result) => {
					if (result.value) {
						$('#myform').submit();
					}
					})
			});
    });
</script>


@endsection