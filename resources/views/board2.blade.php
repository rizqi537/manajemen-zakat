@extends('layouts.app')

@section('title',"Manajemen Zakat")
@section('content')

<div class="block-header">
    <h2>DONASI MASUK</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="info-box bg-indigo hover-expand-effect">
            <div class="icon">
                <i class="material-icons">monetization_on</i>
            </div>
            <div class="content">
                <div class="text"> INFAQ MASUK</div>
                    <p><span class="sales-count-to number count-to" data-from="0" data-to="{{$zinfaq->total_zakat}}" data-speed="1000" data-fresh-interval="20"></span> Rupiah</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">monetization_on</i>
                </div>
                <div class="content">
                    <div class="text">ZAKAT FITRAH MASUK</div>
                        <p><span class="sales-count-to number count-to" data-from="0" data-to="{{$zfitrah->total_zakat}}" data-speed="1000" data-fresh-interval="20"></span> Rupiah</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-indigo hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">monetization_on</i>
                    </div>
                    <div class="content">
                        <div class="text">FIDIAH MASUK</div>
                            <p><span class="sales-count-to number count-to" data-from="0" data-to="{{$zfidyah->total_zakat}}" data-speed="1000" data-fresh-interval="20"></span> Rupiah</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">monetization_on</i>
                        </div>
                        <div class="content">
                            <div class="text">ZAKAT PENGHASILAN MASUK</div>
                                <p><span class="sales-count-to number count-to" data-from="0" data-to="{{$zpenghasilan->total_zakat}}" data-speed="1000" data-fresh-interval="20"></span> Rupiah</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box bg-indigo hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">shop</i>
                            </div>
                            <div class="content">
                                <div class="text">TOTAL DANA ZAKAT MASUK</div>
                                    <p><span class="sales-count-to number count-to" data-from="0" data-to="{{$typezakat->JumlahZakat}}" data-speed="1000" data-fresh-interval="20"></span> Rupiah</p>
                                </div>
                            </div>
                        </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">groups</i>
                    </div>
                    <div class="content">
                        <div class="text">TOTAL MUZZAKI</div>
                            <p><span class="number count-to" data-from="0" data-to="{{$data}}" data-speed="1000" data-fresh-interval="20"></span></p>
                        </div>
                    </div>
                </div>

</div>

<div class="block-header">
    <h2>PENYALURAN ZAKAT</h2>
</div>

<div class="row clearfix">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="info-box bg-indigo hover-expand-effect">
            <div class="icon">
                <i class="material-icons">monetization_on</i>
            </div>
            <div class="content">
                <div class="text">PENYALURAN INFAQ</div>
                    <p><span class="sales-count-to number count-to" data-from="0" data-to="{{$report4}}" data-speed="1000" data-fresh-interval="20"></span> Rupiah</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">monetization_on</i>
                </div>
                <div class="content">
                    <div class="text">PENYALURAN ZAKAT FITRAH</div>
                        <p><span class="sales-count-to number count-to" data-from="0" data-to="{{$report1}}" data-speed="1000" data-fresh-interval="20"></span> Rupiah</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-indigo hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">monetization_on</i>
                    </div>
                    <div class="content">
                        <div class="text">PENYALURAN ZAKAT FIDIAH</div>
                            <p><span class="sales-count-to number count-to" data-from="0" data-to="{{$report2}}" data-speed="1000" data-fresh-interval="20"></span> Rupiah</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">monetization_on</i>
                        </div>
                        <div class="content">
                            <div class="text">PENYALURAN ZAKAT PENGHASILAN</div>
                                <p><span class="sales-count-to number count-to" data-from="0" data-to="{{$report3}}" data-speed="1000" data-fresh-interval="20"></span> Rupiah</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box bg-indigo hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">shop</i>
                            </div>
                            <div class="content">
                                <div class="text">TOTAL DONASI YANG DISALURKAN</div>
                                    <p><span class="sales-count-to number count-to" data-from="0" data-to="{{$totalreport}}" data-speed="1000" data-fresh-interval="20"></span> Rupiah</p>
                                </div>
                            </div>
                        </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">groups</i>
                    </div>
                    <div class="content">
                        <div class="text">TOTAL MUZZAKI</div>
                            <p><span class="number count-to" data-from="0" data-to="{{$data}}" data-speed="1000" data-fresh-interval="20"></span></p>
                        </div>
                    </div>
                </div>
</div>

@endsection