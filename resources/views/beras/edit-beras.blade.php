@extends('layouts.app')

@section('title',"Edit Transaksi Zakat")
@section('content')
<div class="block-header">
    <h2>DATA ZAKAT BERAS</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<div class="card">
        	<div class="header">
                <h2>
                    DATA
                </h2>
            </div>
                    <div class="body">
                    	<form class="form-vertical" id="myform" action="{{route('beras.update', $beras)}}" method="POST">
                    	<h2 class="card-inside-title">DATA ZAKAT BERAS</h2>
							@csrf
                    		{{  method_field('PATCH') }}
							<input type="hidden" name="_idm" value="{{base64_encode($beras->id)}}">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="uang_beras" name="uang_beras" class="form-control only-num" value="{{$beras->uang_beras}}">
                                    <label class="form-label">Harga Beras</label>
                                </div>
                            </div>
                    		<div class="row clearfix">
                                <div class="">
                                <input type="button" name="insert" id="insert" value="Ubah" class="btn btn-primary m-t-15 waves-effect">
                                <input type="reset" class="btn btn-primary m-t-15 waves-effect" value="RESET">
                                </div>
                            </div>
                    	</form>
                    </div>
        </div>
    </div>
</div>
<script>
	$(document).ready( function () {
			$( ".only-num" ).keypress(function(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;
				return true;
			});
			$( ".dec-num" ).keypress(function(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
					return false;
				return true;
			});
			$( "#beras" ).focus(function() {
				var jiwa = $( "#jiwa" ).val();
				var beras = 3.5;
				var res = jiwa*beras;
				$( "#beras" ).val(res);
			});
			$( "#uang" ).focus(function() {
				var jiwa = $( "#jiwa" ).val();
				var nominal = $("#tipe option:selected").val();
				if(nominal == "0"){
					$("#uang").val("0");
				} else {
					$.ajax({
						type:"GET",
						url:"{{url('nominal')}}/"+nominal+"",
						success: function(data) {
							var total = data.nominal*jiwa;
							$("#uang").val(total);
						}
					});
				}
			});
			$("#insert").click(function(){
				swal({
					title: 'Apakah Datanya Sudah Sesuai?',
					text: "Pastikan Data Yang Anda Berikan Sesuai",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Sudah Sesuai',
 					cancelButtonText: 'Tidak, batalkan!',
					}).then((result) => {
					if (result.value) {
						$('#myform').submit();
					}
					})
			});
	} );
</script>
@endsection