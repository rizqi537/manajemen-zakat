<!-- ======= Header ======= -->
  <div id="header-sticky-wrapper" class="sticky-wrapper" style="height: 70px;">
  <header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light"><img src="{{asset("assets/img/")}}/logo2.png" alt="" title="" width="50" height="50">&nbsp; <a href="/"><span>Yayasan Miftahul Falah Al Amaanah</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class=""><a href="/about">Tentang Kami</a></li>

          <li class="drop-down"><a href="#">Tunaikan Zakat</a>
            <ul>
              <li><a href="/zakatfitrah">Zakat Fitrah</a></li>
              <li><a href="/buktifidiah">Upload Bukti Zakat Fidiah</a></li>
              <li><a href="/buktifitrah">Upload Bukti Zakat Fitrah</a></li>
              <li><a href="/buktiinfaq">Upload Bukti Zakat Infaq</li>
              <li><a href="/buktipenghasilan">Upload Bukti Zakat Penghasilan</li>
              <li><a href="/form-penghasilan">Zakat Penghasilan</a></li>
              <li><a href="/zakatfidyah">Fidiah</a></li>
              <li><a href="/zakatinfaq">Infaq</a></li>
            </ul>
          </li>
          <li><a href="/kalkulatorzakat">Kalkulator Zakat</a></li>

            </ul>
          </li>

          <!--li><a href="services.html">Berita</a></li>
          <li><a href="portfolio.html">Galeri</a></li>
          <li><a href="contact.html">Hubungi</a></li-->

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
  </div>