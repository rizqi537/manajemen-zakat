<footer id="footer">
    <div class="footer-top">
        <div class="container">
          <div class="row">
  
            <div class="col-lg-3 col-md-6 footer-links">
              <h4>Links</h4>
              <ul>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="/about">Tentang Kami</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="/zakatfitrah">Zakat Fitrah</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="/zakatfidyah">Fidyah</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="/form-penghasilan">Zakat Penghasilan</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="/zakatinfaq">Infaq</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="/kalkulatorzakat">Kalkulator Zakat</a></li>
              </ul>
            </div>
  
            
  
            <div class="col-lg-4 col-md-6 footer-contact">
              <h4>Hubungi Kami</h4>
              <p>
                Jl.Cibaligo, Cimindi Hilir No.75<br>
                RT.04 RW.30 Cibeureum<br>
                Cimahi Selatan - Kota Cimahi<br><br>
                <strong>Phone:</strong> 081321583666<br>
                <strong>Email:</strong> miftahulfalah1220@gmail.com<br>
                <strong>Facebook:</strong><a href="https://web.facebook.com/miftahulfalah.alamaanah.1"> Yayasan Miftahul Falah Al Amaanah</a><br>
                <strong>Instagram:</strong><a href="https://www.instagram.com/ypp.miftahulfalah/"> @ypp.miftahulfalah</a><br>
                <strong>Youtube:</strong><a href="https://www.youtube.com/channel/UC_MU9GdrgT2iOYnfh3a9OkA"> Yayasan Miftahul Falah Al Amaanah</a><br>
              </p>
  
            </div>

            <!-- About and Social Media Link -->
    
            </div>
          </div>
        </div>
    
        <div class="container">
          <div class="copyright">
            &copy; Copyright <strong><span>Yayasan Miftahul Falah Al Amaanah</span></strong>. All Rights Reserved
          </div>
          <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/ -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
        </div>
</footer>