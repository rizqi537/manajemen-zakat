@extends('layouts.front')

@section('content')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="/">Home</a></li>
          <li>Konfirmasi Pembayaran Penghasilan</li>
        </ol>
        <h2>Konfirmasi Pembayaran Zakat Penghasilan</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
      <div class="container">

          <form action="{{route('penghasilan.bukti', $penghasilan->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            {{  method_field('PATCH') }}
              <label for="bukti">Masukan Bukti Transfer:</label>
              <input type="file" id="bukti" name="bukti">
              <input type="submit">
          </form>
          <br>

        <table class="table table-bordered" style="width: 80%">
            <tr>
                <th>Harga beras yang dikonsumsi :</th>
                <td>{{"Rp. ".number_format($penghasilan->harga_beras,0,'',',').',-'}}</td>
            </tr>
            <tr>
                <th>Total Zakat penghasilan :</th>
                <td>{{"Rp. ".number_format($penghasilan->jumlah_zakat,0,'',',').',-'}}</td>
            </tr>
            <tr>
                <th>Pembayaran Dilakukan ke rekening Bank BJB A.n <b>Miftahul Falah Al Amaanah</b></th>
                <td><b>0113295767101</b></td>
            </tr>     
        </table>
        
        <table class="table table-bordered" style="width: 80%">
            <tr>
                <th>Nama Lengkap :</th>
                <td>{{$penghasilan->nama}}</td>
            </tr>
            <tr>
                <th>No Telepon :</th>
                <td>{{$penghasilan->nohp}}</td>
            </tr>
            <tr>
                <th>Email :</th>
                <td>{{$penghasilan->email}}</td>
            </tr>  
        </table>

      </div>
    </section><!-- End Pricing Section -->

  </main><!-- End #main -->

  @endsection