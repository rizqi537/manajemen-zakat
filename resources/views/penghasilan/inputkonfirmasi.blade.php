@extends('layouts.front')

@section('content')

<main id="main">

    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">
  
          <ol>
            <li><a href="/">Beranda</a></li>
            <li>Zakat</li>
            <li>Zakat Penghasilan</li>
          </ol>
          <h2>Zakat Penghasilan</h2>
  
        </div>
      </section><!-- End Breadcrumbs -->
    <!-- ======= Contact Section ======= -->
    <style>
div {
  margin-bottom: 15px;
  padding: 4px 12px;
}

.info {
  background-color: #e7f3fe;
  border-left: 6px solid #2196F3;
}
</style>
    <section id="contact" class="contact">
      <div class="container">

        <div class="row">

          <div class="col-lg-7">
            <form action="/penghasilan/cari" method="GET">    
                <div class="form-group">
                  <label for="rp">Silahkan masukan nomor telepon</label>
                    <input type="search" name="nohp" class="form-control only-num" placeholder="Nomor Telepon" />
                    <span class="form-group-btn">
                      <button type="submit" class="btn btn-primary">Search</button>
                    </span>
                  </div>
                  <div class="col-lg-12 mt-4 mt-lg-0">
                  </div>
                  <!--<input type="submit" name="insert" id="insert" value="Submit" class="btn btn-primary m-t-15 waves-effect">-->
            </form>
          </div>

        </div>

      </div>

</div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <script type="text/javascript">
  
  </script>

@endsection