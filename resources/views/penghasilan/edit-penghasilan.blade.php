@extends('layouts.app')

@section('title',"Edit Transaksi Zakat Penghasilan")
@section('content')

<div class="block-header">
    <h2>DATA ZAKAT PENGHASILAN</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<div class="card">
        	<div class="header">
                <h2>
                    EDIT ZAKAT PENGHASILAN
                </h2>
            </div>
                    <div class="body">
                    	<form class="form-vertical" id="myform" action="{{route('penghasilan.update', $penghasilan)}}" method="POST" enctype="multipart/form-data">
                    	<h2 class="card-inside-title">DATA ZAKAT PENGHASILAN</h2>
							@csrf
                    		{{  method_field('PATCH') }}
							<input type="hidden" name="_idf" value="{{base64_encode($penghasilan->id)}}">
                    		<div class="form-group form-float">
                    			<div class="form-line">
                    				<select style="width: 200px" class="form-control" id="harga_beras" name="harga_beras">
                                        <option value="{{$penghasilan->harga_beras}}" disabled="true" selected="true"placeholder="Harga Beras yang dikonsumsi" id="harga_beras">{{$penghasilan->harga_beras}}</option>
                                        @foreach($beras as $beras)
                                            <option value="{{$beras->uang_beras}}">{{$beras->uang_beras}}</option>
                                        @endforeach
                                    </select>
                    				<label class="form-label">Harga Beras</label>
                    			</div>
                    		</div>
                    		<div class="form-group form-float">
                    			<div class="form-line">
                    				<input type="email" id="gaji_bulan" name="gaji_bulan" class="form-control only-num" value="{{$penghasilan->gaji_bulan}}">
                    				<label class="form-label">Penghasilan Perbulan</label>
                    			</div>
                            </div>
                            <div class="form-group form-float">
                    			<div class="form-line">
                    				<input type="email" id="wajib" name="wajib" class="form-control" placeholder="Wajib Membayar Zakat ?">
                    			</div>
                    		</div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="jumlah_zakat" name="jumlah_zakat" class="form-control only-num" value="{{$penghasilan->jumlah_zakat}}">
                                    <label class="form-label">Total penghasilan</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="nama" name="nama" class="form-control" value="{{$penghasilan->nama}}">
                                    <label class="form-label">Nama Lengkap</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="nohp" name="nohp" class="form-control only-num" value="{{$penghasilan->nohp}}">
                                    <label class="form-label">Nomor Telepon</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="email" name="email" class="form-control" value="{{$penghasilan->email}}">
                                    <label class="form-label">Email</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label class="form-label">Bukti Transfer</label>
                                <input type="file" id="bukti" name="bukti">
                                <div class="form-line">
                                    <img src="{{asset("uploadpenghasilan/$penghasilan->bukti")}}" alt="" title="" width="500" height="500"></a>
                                </div>
                            </div>
                    		<div class="row clearfix">
                                <div class="">
                                <input type="button" name="insert" id="insert" value="Ubah" class="btn btn-primary m-t-15 waves-effect">
                                <input type="reset" class="btn btn-primary m-t-15 waves-effect" value="RESET">
                                </div>
                            </div>
                    	</form>
                    </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (){
        $( ".only-num" ).keypress(function(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;
				return true;
			});

            $("input").on("keyup", function(){
          var harga_beras = $("#harga_beras").val();
          var gaji = $("#gaji_bulan").val();
          var nishab =  harga_beras*524;
          //$("#nishab").val(nishab);
          var res = 0.025*gaji;
          //$("#jumlah_zakat").val(res);
          if (gaji>nishab){
            $("#wajib").val('Wajib');
            $("#jumlah_zakat").val(res);
            $("#insert").on("click", function(){
                swal({
					title: 'Apakah Datanya Sudah Sesuai?',
					text: "Pastikan Data Yang Anda Berikan Sesuai",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Sudah Sesuai',
 					cancelButtonText: 'Tidak, batalkan!',
					}).then((result) => {
					if (result.value) {
						$('#myform').submit();
					}
					})
                }); 
          }else{
            $("#wajib").val('Tidak Wajib');
            $("#jumlah_zakat").val('0');
            $("#insert").on("click", function(){
            swal({
                title: 'Hasil Nishab tidak Wajib',
                text: "Anda dapat memilih zakat Infaq",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<a href="/infaq">Halaman Zakat Infaq</a>',
                cancelButtonText: 'Tidak, batalkan!',
                });
              });   
          }
      })


    });
</script>
@endsection