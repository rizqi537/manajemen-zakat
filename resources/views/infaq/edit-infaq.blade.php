@extends('layouts.app')

@section('title',"Edit Transaksi infaq")
@section('content')
<div class="block-header">
    <h2>DATA INFAQ</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<div class="card">
        	<div class="header">
                <h2>
                    DATA INFAQ
                </h2>
            </div>
                    <div class="body">
                    	<form class="form-vertical" id="myform" action="{{route('infaq.update', $infaq)}}"" method="POST" enctype="multipart/form-data">
                    	<h2 class="card-inside-title">DATA INFAQ</h2>
							@csrf
                    		{{  method_field('PATCH') }}
							<input type="hidden" name="_idf" value="{{base64_encode($infaq->id)}}">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="totalfaq" name="totalfaq" class="form-control only-num" value="{{$infaq->totalfaq}}">
                                    <label class="form-label">Jumlah Infaq</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="nama" name="nama" class="form-control" value="{{$infaq->nama}}">
                                    <label class="form-label">Nama Lengkap</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="nohp" name="nohp" class="form-control only-num" value="{{$infaq->nohp}}">
                                    <label class="form-label">Nomor Telepon</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="email" name="email" class="form-control" value="{{$infaq->email}}">
                                    <label class="form-label">Email</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label class="form-label">Bukti Transfer</label>
                                <input type="file" id="bukti" name="bukti">
                                <div class="form-line">
                                    <img src="{{asset("uploadinfaq/$infaq->bukti")}}" alt="" title="" width="300" height="300"></a>
                                </div>
                            </div>
                    		<div class="row clearfix">
                                <div class="">
                                <input type="button" name="insert" id="insert" value="Ubah" class="btn btn-primary m-t-15 waves-effect">
                                <input type="reset" class="btn btn-primary m-t-15 waves-effect" value="RESET">
                                </div>
                            </div>
                    	</form>
                    </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (){
        $("#insert").click(function(){
                swal({
                    title: 'Apakah Datanya Sudah Sesuai?',
                    text: "Pastikan Data Yang Anda Berikan Sesuai",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Sudah Sesuai',
                    cancelButtonText: 'Tidak, batalkan!',
                    }).then((result) => {
                    if (result.value) {
                        $('#myform').submit();
                    }
                    })
			});
    });
</script>
@endsection