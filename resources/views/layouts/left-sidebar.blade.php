    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            @guest
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{ asset('logo.png')}}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">MIFTAHUL FALAH</div>
                    <div class="email">Tamu</div>
                </div>
            </div>
            <div class="menu">
                <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="{{route('home')}}">
                            <i class="material-icons">dashboard</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #User Info -->
            @else
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{ asset('profile.jpg') }}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}}</div>
                    <div class="email">{{Auth::user()->email}}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="{{route('profil.edit')}}"><i class="material-icons">person</i>Profile</a></li>
                            @if(Auth::user()->hasRole('Administrator'))
                                <li><a href="{{ route('user.history') }}"><i class="material-icons">list</i>History Login</a></li>
                            @endif
                            <li role="seperator" class="divider"></li>
                            <li><a href="{{ route('password.change') }}"><i class="material-icons">https</i>Ganti Password</a></li>
                            
                            {{--  <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>  --}}
                            {{--  <li role="seperator" class="divider"></li>  --}}
                            {{--  <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>  --}}
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    
                    <li class="{{ (\Request::route()->getName() == 'board2') ? 'active' : '' }}">
                        <a href="{{route('board2')}}">
                            <i class="material-icons">dashboard</i>
                            <span>Dashboard v2</span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">dehaze</i>
                            <span>Kelola Zakat</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ (\Request::route()->getName() == 'pengeluaran' || \Request::route()->getName() == 'pengeluaran.edit' || \Request::route()->getName() == 'beras.createOther' || \Request::route()->getName() == 'beras.confirmation' || \Request::route()->getName() == 'beras.show' || \Request::route()->getName() == 'jeniszakat.change') ? 'active' : '' }}">
                                <a href="{{route('pengeluaran')}}">
                                    <span>Pengeluaran</span>
                                </a>
                            </li>
                            <li class="{{ (\Request::route()->getName() == 'penghasilan' || \Request::route()->getName() == 'penghasilan.create' || \Request::route()->getName() == 'penghasilan.createOther' || \Request::route()->getName() == 'penghasilan.confirmation' || \Request::route()->getName() == 'penghasilan.show' ) ? 'active' : '' }}">
                                <a href="{{route('penghasilan')}}">
                                    <span>Zakat Penghasilan</span>
                                </a>
                            </li>
                            <li class="{{ (\Request::route()->getName() == 'infaq' || \Request::route()->getName() == 'infaq.create' || \Request::route()->getName() == 'infaq.createOther' || \Request::route()->getName() == 'infaq.confirmation' || \Request::route()->getName() == 'infaq.show' ) ? 'active' : '' }}">
                                <a href="{{route('infaq')}}">
                                    <span>Infaq</span>
                                </a>
                            </li>
                            <li class="{{ (\Request::route()->getName() == 'fitrah' || \Request::route()->getName() == 'fitrah.create' || \Request::route()->getName() == 'fitrah.createOther' || \Request::route()->getName() == 'fitrah.confirmation' || \Request::route()->getName() == 'fitrah.show' ) ? 'active' : '' }}">
                                <a href="{{route('fitrah')}}">
                                    <span>Zakat Fitrah</span>
                                </a>
                            </li>
                            <li class="{{ (\Request::route()->getName() == 'fidyah' || \Request::route()->getName() == 'fidyah.create' || \Request::route()->getName() == 'fidyah.createOther' || \Request::route()->getName() == 'fidyah.confirmation' || \Request::route()->getName() == 'fidyah.show' || \Request::route()->getName() == 'jeniszakat.change') ? 'active' : '' }}">
                                <a href="{{route('fidyah')}}">
                                    <span>Fidiah</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    @can('isAdmin')
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">dehaze</i>
                            <span>Laporan</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ (\Request::route()->getName() == 'beras' || \Request::route()->getName() == 'beras.create' || \Request::route()->getName() == 'beras.createOther' || \Request::route()->getName() == 'beras.confirmation' || \Request::route()->getName() == 'beras.show' || \Request::route()->getName() == 'jeniszakat.change') ? 'active' : '' }}">
                                <a href="{{route('beras')}}">
                                    <span>Data Harga Beras</span>
                                </a>
                            </li>
                            <li class="{{ (\Request::route()->getName() == 'mustahiq') ? 'active' : '' }}">
                                <a href="{{route('mustahiq')}}">
                                    <span>Jenis Mustahiq</span>
                                </a>
                            </li>
                            <li class="{{ (\Request::route()->getName() == 'typezakat' || \Request::route()->getName() == 'artikel.show') ? 'active' : '' }}">
                                <a href="{{route('typezakat')}}">
                                    <span>Total Dana Zakat Masuk</span>
                                </a>
                            </li>
                            <li class="{{ (\Request::route()->getName() == 'report3') || (\Request::route()->getName() == 'pengeluaran') ? 'active' : '' }}">
                                <a href="{{route('report3')}}">
                                    <span>Laporan Pemasukan Zakat</span>
                                </a>
                            </li> 
                            <li class="{{ (\Request::route()->getName() == 'report') || (\Request::route()->getName() == 'pengeluaran') ? 'active' : '' }}">
                                <a href="{{route('report')}}">
                                    <span>Laporan Pengeluaran</span>
                                </a>
                            </li>  
                        </ul>
                    </li>
                    @endcan
                    
                    <li class="{{ (\Request::route()->getName() == 'artikel' || \Request::route()->getName() == 'artikel.show') ? 'active' : '' }}">
                        <a href="{{route('artikel')}}">
                            <i class="material-icons">description</i>
                            <span>Artikel</span>
                        </a>
                    </li>
                    
                    <!-- Hide -->
                    
                    <li class="{{ (\Request::route()->getName() == 'user' || \Request::route()->getName() == 'profil.edit' || \Request::route()->getName() == 'password.change' || \Request::route()->getName() == 'role.edit' || \Request::route()->getName() == 'user.history') ? 'active' : '' }}">
                        @if(Auth::user()->hasRole('Administrator'))
                            <a href="{{route('user')}}">
                        @else
                            <a href="{{route('profil.edit')}}">
                        @endif
                            <i class="material-icons">account_box</i>
                            <span>User</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; {{date('Y')}} <a href="javascript:void(0);">{{ config('app.name', 'Laravel') }}</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 2.1
                </div>
            </div>
            <!-- #Footer -->
            @endguest
        </aside>
        <!-- #END# Left Sidebar -->
    </section>