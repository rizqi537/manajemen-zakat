<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <meta content="" name="descriptison">
        <meta content="" name="keywords">

        <title>Miftahul Falah</title>

        <!-- Favicons -->
        <link href= "{{asset("assets/img/")}}/favicon2.png" rel="icon">
        <link href="{{asset("assets/img/")}}/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="{{asset("assets/vendor/bootstrap/css/")}}/bootstrap.min.css" rel="stylesheet">
        <link href="{{asset("assets/vendor/icofont/")}}/icofont.min.css" rel="stylesheet">
        <link href="{{asset("assets/vendor/boxicons/css/")}}/boxicons.min.css" rel="stylesheet">
        <link href="{{asset("assets/vendor/animate.css/")}}/animate.min.css" rel="stylesheet">
        <link href="{{asset("assets/vendor/owl.carousel/assets/")}}/owl.carousel.min.css" rel="stylesheet">
        <link href="{{asset("assets/vendor/venobox/")}}/venobox.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="{{asset("assets/css/")}}/style.css" rel="stylesheet">

        <!-- Vendor JS Files -->
        <script src="{{asset("assets/vendor/jquery/")}}/jquery.min.js"></script>
        <script src="{{asset("assets/vendor/bootstrap/js/")}}/bootstrap.bundle.min.js"></script>
        <script src="{{asset("assets/vendor/jquery.easing/")}}/jquery.easing.min.js"></script>
        <script src="{{asset("assets/vendor/php-email-form/")}}/validate.js"></script>
        <script src="{{asset("assets/vendor/jquery-sticky/")}}/jquery.sticky.js"></script>
        <script src="{{asset("assets/vendor/owl.carousel/")}}/owl.carousel.min.js"></script>
        <script src="{{asset("assets/vendor/waypoints/")}}/jquery.waypoints.min.js"></script>
        <script src="{{asset("assets/vendor/counterup/")}}/counterup.min.js"></script>
        <script src="{{asset("assets/vendor/isotope-layout/")}}/isotope.pkgd.min.js"></script>
        <script src="{{asset("assets/vendor/venobox/")}}/venobox.min.js"></script>

        <!-- Template Main JS File -->
        <script src="{{asset("assets/js/")}}/main.js"></script>

        <!-- Sweetalert2 Js -->
        <script src="https://unpkg.com/sweetalert2@7.19.3/dist/sweetalert2.all.js"></script>
        <script src="https://unpkg.com/promise-polyfill@7.1.0/dist/promise.min.js"></script>

        <style>
            #header {
                position: fixed !important;
                width: 100%;
            }
        </style>
    </head>
    <body>
        @include('halamanfront.navbarpublic')
        @yield('content')
        @include('halamanfront.footerpublic')
        @include('sweetalert::alert')
    </body>
</html>