<doctype html>
    <html>
       <head>
          <meta charset="utf-8">
          <title>Nota Pembayaran Untuk {{$fitrah->nama}}</title>
          <style> 	     .invoice-box{ 	    background-color: #FFFFFF;         max-width:800px;         margin:30px 0;         padding:30px;         border:1px solid #eee;         box-shadow:0 0 10px rgba(0, 0, 0, .15);         font-size:16px;         line-height:24px;         font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;         color:#555;     } 	 	.invoice-boxx{ 	    background-image: url("http://images.alphacoders.com/458/458169.jpg");         max-width:800px;         margin:auto;         padding:30px;         border:1px solid #eee;         box-shadow:0 0 10px rgba(0, 0, 0, .15);         font-size:16px;         line-height:24px;         font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;         color:#f7f7f7;     } 	 .btn {   background: #3cb0fd;   background-image: -webkit-linear-gradient(top, #3cb0fd, #3cb0fd);   background-image: -moz-linear-gradient(top, #3cb0fd, #3cb0fd);   background-image: -ms-linear-gradient(top, #3cb0fd, #3cb0fd);   background-image: -o-linear-gradient(top, #3cb0fd, #3cb0fd);   background-image: linear-gradient(to bottom, #3cb0fd, #3cb0fd);   -webkit-border-radius: 4;   -moz-border-radius: 4;   border-radius: 4px;   font-family: Arial;   color: #ffffff;   font-size: 35px;   padding: 6px 16px 10px 20px;   text-decoration: none; }  .btn:hover {   background: #3cb0fd;   background-image: -webkit-linear-gradient(top, #3cb0fd, #3cb0fd);   background-image: -moz-linear-gradient(top, #3cb0fd, #3cb0fd);   background-image: -ms-linear-gradient(top, #3cb0fd, #3cb0fd);   background-image: -o-linear-gradient(top, #3cb0fd, #3cb0fd);   background-image: linear-gradient(to bottom, #3cb0fd, #3cb0fd);   text-decoration: none; }  	     .invoice-box table{         width:100%;         line-height:inherit;         text-align:left;     }          .invoice-box table td{         padding:5px;         vertical-align:top;     }          .invoice-box table tr td:nth-child(2){         text-align:right;     }          .invoice-box table tr.top table td{         padding-bottom:20px;     }          .invoice-box table tr.top table td.title{         font-size:45px;         line-height:45px;         color:#333;     }          .invoice-box table tr.information table td{         padding-bottom:40px;     }          .invoice-box table tr.heading td{         background:#EEEEE0;         border-bottom:1px solid #ddd;         font-weight:bold;     }          .invoice-box table tr.details td{         padding-bottom:20px;     }          .invoice-box table tr.item td{         border-bottom:1px solid #eee;     }          .invoice-box table tr.item.last td{         border-bottom:none;     }          .invoice-box table tr.total td:nth-child(2){         border-top:2px solid #eee;         font-weight:bold;     }          @media only screen and (max-width: 600px) {         .invoice-box table tr.top table td{             width:100%;             display:block;             text-align:center;         }                  .invoice-box table tr.information table td{             width:100%;             display:block;             text-align:center;         }     }     </style>
       </head>
       @php
        $val = array($fitrah->uang_beras,$fitrah->jumlah_jiwa,$fitrah->totalfi,$fitrah->nama,$fitrah->nohp,$fitrah->email);
        $data = array_sum($val);
       @endphp
       <body>
          <div class="invoice-boxx">
          <h1 align = "center">Yayasan Miftahul Falah Al Amaanah</h1>
          <div class="invoice-box">
             <table cellpadding="0" cellspacing="0">
             <tr class="top">
                <td colspan="2">
                   <table>
                      <tr>
                         <td class="title"><img src="{{ asset('assets/img/logo2.png')}}" style="width:50%; max-width:100px;"></td>
                         <td> No Nota: NOTA-00{{$fitrah->id}}<br>
                              Cimahi, {{\UserAgent::tanggalIndo(date('Y-m-d'))}}<br>
                              {{--  Due: {{{invoice_date_due}}}                             --}}
                          </td>
                      </tr>
                   </table>
                </td>
             </tr>
             <tr class="information">
                <td>                     
                  <tr>
                      <td>
                        Yayasan Miftahul Falah Al Amaanah<br>
                        Jln. Cibaligo, Gg. Cimindi Hilir no. 75, Rt.04 Rw.30 <br>
                        Kel. Cibeureum Kec. Cimahi selatan Kota Cimahi, Kode pos 40535<br>
                        Bandung<br>                             
                      </td>
                      <td>
                        <strong>Diberikan Kepada:</strong><br>                               
                        {{$fitrah->nama}}<br> 								
                        {{$fitrah->email}}<br>
                      </td>
                  </tr>
                </td>	    		    
              </tr> 			
             <table>
                <tr class="heading">
                   <td>                                      </td>
                   <td>                     Jumlah                 </td>
                </tr>
    
              @isset($fitrah->jumlah_jiwa)
                <tr class="details">
                  <td>               
                    Jumlah Jiwa                 
                  </td>
                  <td>                     
                    {{$fitrah->jumlah_jiwa}}                 
                  </td>
                </tr>
              @endisset
    
              @isset($fitrah->totalfit)
                <tr class="details">
                  <td>               
                    Total Zakat Fitrah                
                  </td>
                  <td>                     
                    Rp. {{number_format($fitrah->totalfit,0,'',',')}}               
                  </td>
                </tr>
              @endisset
    
              @if($data > 0)
                <tr class="total">
                  <td></td>
                  <td>                    
                    Total: Rp. {{number_format($fitrah->totalfit,0,'',',')}}                 
                  </td>
                </tr>
              @endif
             </table>
             <p align="center"><strong>Terima Kasih Telah Mempercayai Kami</strong></p>
          </div>
          </hr> 	
          <div></hr></div>
          <div class="invoice-box">
          <h1 align="center">Yayasan Miftahul Falah Al Amaanah</h1>
          <p align="center"> 
            {{--  Phone: +62 81321583666<br>    --}}
            Email: miftahulfalah1220@gmail.com<br>
          </p>
          <div align="center">
            <a href="https://web.facebook.com/miftahulfalah.alamaanah.1"> 				
              <img src="https://cdn0.iconfinder.com/data/icons/free-social-media-set/24/facebook-128.png" align="middle" style="width:10%; max-width:50px;"> 			
            </a> 			
            <a href="https://www.instagram.com/ypp.miftahulfalah/"> 				
              <img src="https://cdn0.iconfinder.com/data/icons/free-social-media-set/24/instagram-128.png" align="middle" style="width:10%; max-width:50px;" > 			
            </a> 	 			
            <a href="https://www.youtube.com/channel/UC_MU9GdrgT2iOYnfh3a9OkA"> 				
              <img src="https://cdn0.iconfinder.com/data/icons/free-social-media-set/24/youtube2-128.png" align="middle" style="width:10%; max-width:50px;" > 			
            </a> 
          </div>
          <div align="center"> 		
            <a href="https://web.facebook.com/miftahulfalah.alamaanah.1">Yayasan Miftahul Falah Al Amaanah.</a> 	
          </div>
       </body>
    </html>