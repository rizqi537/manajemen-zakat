@extends('layouts.front')

@section('content')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="/">Home</a></li>
          <li>Konfirmasi Pembayaran Zakat Fitrah</li>
        </ol>
        <h2>Konfirmasi Pembayaran Zakat Fitrah</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
      <div class="container">

          <form action="{{route('fitrah.bukti', $fitrah->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            {{  method_field('PATCH') }}
              <label for="bukti">Masukan Bukti Transfer:</label>
              <input type="file" id="bukti" name="bukti">
              <input type="submit">
          </form>
          <br>

        <table class="table table-bordered" style="width: 80%">
            <tr>
                <th>Jumlah Jiwa :</th>
                <td>{{$fitrah->jumlah_jiwa}}</td>
            </tr>
            <tr>
                <th>Total Zakat fitrah :</th>
                <td>{{"Rp. ".number_format($fitrah->totalfit,0,'',',').',-'}}</td>
            </tr>
            <tr>
              <th>Total Zakat Infaq :</th>
              <td>{{"Rp. ".number_format($fitrah->jumlah_infaq,0,'',',').',-'}}</td>
          </tr>
            <tr>
                <th>Pembayaran Dilakukan ke rekening Bank BJB A.n <b>Miftahul Falah Al Amaanah</b></th>
                <td><b>0113295767101</b></td>
            </tr>     
        </table>
        
        <table class="table table-bordered" style="width: 80%">
            <tr>
                <th>Nama Lengkap :</th>
                <td>{{$fitrah->nama}}</td>
            </tr>
            <tr>
                <th>No Telepon :</th>
                <td>{{$fitrah->nohp}}</td>
            </tr>
            <tr>
                <th>Email :</th>
                <td>{{$fitrah->email}}</td>
            </tr>  
        </table>

      </div>
    </section><!-- End Pricing Section -->

  </main><!-- End #main -->

  @endsection