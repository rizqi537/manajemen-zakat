@extends('layouts.app')

@section('title',"Edit Transaksi Fitrah")
@section('content')

<div class="block-header">
    <h2>DATA ZAKAT FITRAH</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<div class="card">
        	<div class="header">
                <h2>
                    EDIT ZAKAT FITRAH
                </h2>
            </div>
                    <div class="body">
                    	<form class="form-vertical" id="myform" action="{{route('fitrah.update', $fitrah)}}" method="POST" enctype="multipart/form-data">
                    	<h2 class="card-inside-title">DATA ZAKAT fitrah</h2>
							@csrf
                    		{{  method_field('PATCH') }}
							<input type="hidden" name="_idf" value="{{base64_encode($fitrah->id)}}">
                    		<div class="form-group form-float">
                    			<div class="form-line">
                    				<select style="width: 200px" class="form-control" id="harga_beras" name="harga_beras">
                                        <option value="0" disabled="true" selected="true" id="harga_beras">-- Harga Beras --</option>
                                        @foreach($beras as $beras)
                                            <option value="{{$beras->uang_beras}}">{{$beras->uang_beras}}</option>
                                        @endforeach
                                    </select>
                    				<label class="form-label">Harga Beras</label>
                    			</div>
                    		</div>
                    		<div class="form-group form-float">
                    			<div class="form-line">
                    				<input type="email" id="jumlah_jiwa" name="jumlah_jiwa" class="form-control" value="{{$fitrah->jumlah_jiwa}}">
                    				<label class="form-label">Jumlah Jiwa</label>
                    			</div>
                    		</div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="totalfit" name="totalfit" class="form-control only-num" value="{{$fitrah->totalfit}}">
                                    <label class="form-label">Total fitrah</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="nama" name="nama" class="form-control" value="{{$fitrah->nama}}">
                                    <label class="form-label">Nama Lengkap</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="nohp" name="nohp" class="form-control only-num" value="{{$fitrah->nohp}}">
                                    <label class="form-label">Nomor Telepon</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="email" name="email" class="form-control" value="{{$fitrah->email}}">
                                    <label class="form-label">Email</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label class="form-label">Bukti Transfer</label>
                                <input type="file" id="bukti" name="bukti">
                                <div class="form-line">
                                    <img src="{{asset("uploadfitrah/$fitrah->bukti")}}" alt="" title="" width="500" height="500"></a>
                                </div>
                            </div>
                    		<div class="row clearfix">
                                <div class="">
                                <input type="button" name="insert" id="insert" value="Ubah" class="btn btn-primary m-t-15 waves-effect">
                                <input type="reset" class="btn btn-primary m-t-15 waves-effect" value="RESET">
                                </div>
                            </div>
                    	</form>
                    </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (){
        $( ".only-num" ).keypress(function(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
				return true;
			});
            $("input").on("keyup", function(){
                var harga_beras = $("#harga_beras").val();
                var jumlah_jiwa = $("#jumlah_jiwa").val();
                var res = harga_beras*2.5*jumlah_jiwa;
            $("#totalfit").val(res);
        });
        $("#insert").click(function(){
				swal({
					title: 'Apakah Datanya Sudah Sesuai?',
					text: "Pastikan Data Yang Anda Berikan Sesuai",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Sudah Sesuai',
 					cancelButtonText: 'Tidak, batalkan!',
					}).then((result) => {
					if (result.value) {
						$('#myform').submit();
					}
					})
			});

    });
</script>
@endsection