@extends('layouts.app')

@section('title', 'Data Zakat Penghasilan')
@section('content')
<div class="block-header">
            <h2>DAFTAR ZAKAT MASUK</h2>
        </div>
        <div class="row clearfix">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        		<div class="card">
        			<div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-11">
                                <h2>
                                    DATA ZAKAT MASUK
                                </h2>  
                            </div>
                        </div>
        			</div>
                    <div class="body">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="tballzakat">
                                <thead>
                                    <tr>
                                        <th>NAMA ZAKAT</th>
                                        <th>TOTAL ZAKAT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
        		</div>
        	</div>
        </div>
        <div class="modal fade" id="insertModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Tambah Data Zakat</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('typezakat.store')}}" method="post" id="artikel" enctype="multipart/form-data">
                            @csrf
                            <div class="row clearfix">
                                    <div class="form-group form-float">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="nama_zakat" id="nama_zakat" class="form-control" required="">
                                                <label class="form-label">Nama Zakat</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <input type="button" name="insert" id="insert" value="TAMBAH" class="btn btn-primary m-t-15 waves-effect">
                                    </div>
                                </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            jQuery(document).ready(function(){
                var table = $('#tballzakat').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    buttons: [
                        {
                            extend: 'print',
                            footer: false,
                            exportOptions: {
                                columns: [0,1]
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: false,
                            exportOptions: {
                                columns: [0,1]
                            }
                            
                        },
                        {
                            extend: 'excel',
                            footer: false,
                            exportOptions: {
                                columns: [0,1]
                            }
                        } 
                    ],
                    ajax: {
                        url: '{{ url("typezakat-list") }}'
                    },
                    columns: [
                    {data: 'nama_zakat', name: 'nama_zakat'},
                    {data: 'total_zakat', name: 'total_zakat'},
                    ],
                });
                $("#insert").on("click", function(){
                    swal({
                        title: 'Apakah Data Sudah Sesuai?',
                        text: "Anda Dapat Mengedit Kembali",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Saya Yakin',
                        cancelButtonText: 'Tidak, batalkan!',
                        }).then((result) => {
                        if (result.value) {
                            $('#artikel').submit();
                        }
					})
                });
                $("#tbzakatpenghasilan").on("click", "#apus", function(e){
                    var csrf_token = $('meta[name="csrf-token"]').attr("content");
                    var id = $(this).data("value");
                    swal({
                        title: 'Apakah Kamu Yakin Ingin Dihapus?',
                        text: "Data Tidak Bisa Dikembalikan",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Saya Yakin',
                        cancelButtonText: 'Tidak, batalkan!',
                        }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                url: "{{ url('penghasilan/delete') }}"+ '/' + id,
                                type: "POST",
                                data : {'_method' : 'DELETE', '_token' : csrf_token},
                                success : function(){
                                    table.ajax.reload();
                                }
                            })
                        }
					})
                }); 
            });
        </script>
@endsection