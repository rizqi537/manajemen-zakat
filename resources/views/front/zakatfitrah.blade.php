@extends('layouts.front')

@section('content')

<main id="main">

    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">
  
          <ol>
            <li><a href="/">Beranda</a></li>
            <li>Zakat</li>
            <li>Zakat Fitrah</li>
          </ol>
          <h2>Zakat Fitrah</h2>
  
        </div>
      </section><!-- End Breadcrumbs -->
    <!-- ======= Contact Section ======= -->
    <style>
div {
  margin-bottom: 15px;
  padding: 4px 12px;
}

.info {
  background-color: #e7f3fe;
  border-left: 6px solid #2196F3;
}
</style>
    <section id="contact" class="contact">
      <div class="container">

        <div class="row">

          <div class="col-lg-7">
            <form action="{{route('fitrah.store')}}" method="post" role="form" id="addfitrah">
              @csrf
              <select style="width: 200px" class="form-control" id="harga_beras" name="harga_beras">
                  <option value="0" disabled="true" selected="true" id="harga_beras">-- Harga Beras --</option>
                  @foreach($beras as $beras)
                      <option value="{{$beras->uang_beras}}">{{$beras->uang_beras}}</option>
                  @endforeach
              </select>
              <br>
              <div class="form-group">
                <input type="text" name="jumlah_jiwa" class="form-control only-num" id="jumlah_jiwa" placeholder="Masukan Jumlah Jiwa" data-rule="minlen:4" data-msg="Masukan Jumlah Jiwa"/>
                <span class="validate">
                  <strong style="color:red">{{ $errors->first('jumlah_jiwa') }}</strong>
              </span>
              </div>
              <div class="form-group">
                <input type="text" name="totalfit" class="form-control totalfit only-num" id="totalfit" placeholder="Total Zakat Fitrah" data-rule="minlen:4" data-msg="Total Hargs Beras"/>
                <span class="validate">
                  <strong style="color:red">{{ $errors->first('totalfit') }}</strong>
              </span>
              </div>
              <div class="form-group">
                <input type="text" name="jumlah_infaq" class="form-control jumlah_infaq only-num" id="jumlah_infaq" placeholder="(Optional) Zakat Infaq" />
              </div>
              <br>     
              <div class="form-group">
                  <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Lengkap" data-rule="minlen:4" data-msg="Masukan nama lengkap Anda" />
                  <span class="validate">
                    <strong style="color:red">{{ $errors->first('nama') }}</strong>
                </span>
                </div>
                <div class="form-group">
                    <input type="text" name="nohp" class="form-control only-num" id="nohp" placeholder="Nomor Telepon" data-rule="minlen:4" data-msg="Masukan nomor Telepon Anda" />
                  </div>
                  <div class="form-group">
                    <input type="text" name="email" class="form-control" id="email" placeholder="Alamat E-mail" data-rule="minlen:4" data-msg="Masukan Alamat E-mail Anda" />
                    <span class="validate">
                      <strong style="color:red">{{ $errors->first('email') }}</strong>
                  </span>
                  </div>
                  <div class="col-lg-12 mt-4 mt-lg-0">
                  <div class="info">
                    <p><strong></strong> Sebelum melanjutkan ke menu pembayaran sebaiknya telah melakukan pembayaran ke rekening Bank BJB Syariah A.n <b>Miftahul Falah Al Amaanah</b> No. Rek: <b>0113295767101</b></p>
                  </div>
                  </div>
                  <input type="button" name="insert" id="insert" value="Lakukan Pembayaran" class="btn btn-primary m-t-15 waves-effect">
            </form>
          </div>

        </div>

      </div>

</div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <script type="text/javascript">
    $(document).ready(function(){


      $( ".only-num" ).keypress(function(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;
				return true;
			});

      $("input").on("keyup", function(){
          var harga_beras = $("#harga_beras").val();
          var jumlah_jiwa = $("#jumlah_jiwa").val();
          var res = harga_beras*2.5*jumlah_jiwa;
        $("#totalfit").val(res);
      })

      $("#insert").on("click", function(){
            swal({
                title: 'Apakah Data Sudah Sesuai?',
                text: "Anda Dapat Mengedit Kembali",
                imageUrl: 'zakatfitrah.jpg',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Saya Yakin',
                cancelButtonText: 'Tidak, batalkan!',
                }).then((result) => {
                if (result.value) {
                    $('#addfitrah').submit();
                    }
          });
                });
      
      
      /*$("#totalfitrah").focus(function(){
            var uang_beras = $("#uang_beras").val();
            var jiwa = $("#jiwa").val();
            var res = uang_beras*jiwa;
            $("#totalfitrah").val(res)
        });
        */

        /*$(document).on('change', '.jiwa',function(){
            var jiwa_id=$(this).val();

            var a=$(this).parent();
            console.log(jiwa_id);
            $.ajax({
              type: 'get',
              url: '{!!URL::to('findzakatberas')!!}',
              data:{'id':jiwa_id},
              dataType:'json',
              success:function(data){
                //console.log("kg");
                //console.log("uang_beras")
                //console.log(data.kg);
                a.find('.kg').val(data.kg);
                a.find('.uang_beras').val(data.uang_beras);
              },
              error:function(){

              }
            })
        });*/

    });
  </script>

@endsection