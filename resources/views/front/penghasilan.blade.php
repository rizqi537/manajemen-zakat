@extends('layouts.front')

@section('content')

<main id="main">

    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">
  
          <ol>
            <li><a href="/">Beranda</a></li>
            <li>Zakat</li>
            <li>Zakat Penghasilan</li>
          </ol>
          <h2>Zakat Penghasilan</h2>
  
        </div>
      </section><!-- End Breadcrumbs -->
    <!-- ======= Contact Section ======= -->
    <style>
div {
  margin-bottom: 15px;
  padding: 4px 12px;
}

.info {
  background-color: #e7f3fe;
  border-left: 6px solid #2196F3;
}
</style>
    <section id="contact" class="contact">
      <div class="container">

        <div class="row">

          <div class="col-lg-7">
            <form action="{{route('penghasilan.store')}}" method="post" role="form" id="addpenghasilan">
              @csrf
              <label for="rp">Harga Beras yang dikonsumsi</label>
              <select style="width: 200px" class="form-control" id="harga_beras" name="harga_beras">
                <option value="0" disabled="true" selected="true" id="harga_beras">-- Harga Beras --</option>
                @foreach($beras as $beras)
                    <option value="{{$beras->uang_beras}}">{{$beras->uang_beras}}</option>
                @endforeach
            </select>
              <br>
              <div class="form-group">
                <label for="rp">Penghasilan/Gaji per Bulan</label>
                <input type="text" name="gaji_bulan" class="form-control only-num" id="gaji_bulan" placeholder="0" data-rule="minlen:4" data-msg="Masukan Jumlah Jiwa"/>
                <span class="validate">
                  <strong style="color:red">{{ $errors->first('gaji_bulan') }}</strong>
              </span>
              </div>
              <div class="form-group">
                <label for="rp"><b>Wajib Membayar Zakat ? </b></label>
                <input type="text" name="wajib" class="form-control" id="wajib" placeholder="Keterangan" data-rule="minlen:4" data-msg="Masukan Jumlah Jiwa"/>
                <span class="validate">
                  <strong style="color:red">{{ $errors->first('wajib') }}</strong>
              </span>
              </div>
              <!--<div class="form-group">
                <label for="rp"><b>Jumlah Nisab </b></label>
                <input type="text" name="nishab" class="form-control only-num" id="nishab" placeholder="0" data-rule="minlen:4" data-msg="Masukan Jumlah Jiwa"/>
                <div class="validate"></div>
              </div>-->
              <div class="form-group">
                <label for="rp"><b>Jumlah Zakat</b></label>
                <input type="text" name="jumlah_zakat" class="form-control only-num" id="jumlah_zakat" placeholder="0" data-rule="minlen:4" data-msg="Masukan Jumlah Jiwa"/>
                <span class="validate">
                  <strong style="color:red">{{ $errors->first('jumlah_zakat') }}</strong>
              </span>
              </div>
              <div class="form-group">
                <label for="rp"><b>Nama</b></label>
                <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Lengkap" data-rule="minlen:4" data-msg="Masukan nama lengkap Anda" />
                <span class="validate">
                  <strong style="color:red">{{ $errors->first('nama') }}</strong>
              </span>
              </div>
              <div class="form-group">
                <label for="rp"><b>Nomor Telepon</b></label>
                  <input type="text" name="nohp" class="form-control only-num" id="nohp" placeholder="Nomor Telepon" data-rule="minlen:4" data-msg="Masukan nomor Telepon Anda" />
                  <div class="validate"></div>
                </div>
                <div class="form-group">
                  <label for="rp"><b>Email</b></label>
                  <input type="text" name="email" class="form-control" id="email" placeholder="Alamat E-mail" data-rule="minlen:4" data-msg="Masukan Alamat E-mail Anda" />
                  <span class="validate">
                    <strong style="color:red">{{ $errors->first('email') }}</strong>
                </span>
                </div>
                  <div class="col-lg-12 mt-4 mt-lg-0">
                  <div class="info">
                    <p><strong></strong> Sebelum melanjutkan ke menu pembayaran sebaiknya telah melakukan pembayaran ke rekening Bank BJB Syariah A.n <b>Miftahul Falah Al Amaanah</b> No. Rek: <b>0113295767101</b></p>
                  </div>
                </div>
              <input type="button" name="insert" id="insert" value="Lakukan Pembayaran" class="btn btn-primary m-t-15 waves-effect">
            </form>
          </div>
        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->
  <script type="text/javascript">
    $(document).ready(function(){

      $( ".only-num" ).keypress(function(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;
				return true;
			});


        $("input").on("keyup", function(){
          var harga_beras = $("#harga_beras").val();
          var gaji = $("#gaji_bulan").val();
          var nishab =  harga_beras*524;
          //$("#nishab").val(nishab);
          var res = 0.025*gaji;
          //$("#jumlah_zakat").val(res);
          if (gaji>nishab){
            $("#wajib").val('Wajib');
            $("#jumlah_zakat").val(res);
            $("#insert").on("click", function(){
            swal({
                title: 'Apakah Data Sudah Sesuai?',
                text: "Anda Dapat Mengedit Kembali",
                imageUrl: 'zakatfitrah.jpg',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Saya Yakin',
                cancelButtonText: 'Tidak, batalkan!',
                }).then((result) => {
                if (result.value) {
                    $('#addpenghasilan').submit();
                    }
          });
                }); 
          }else{
            $("#wajib").val('Tidak Wajib');
            $("#jumlah_zakat").val('0');
            $("#insert").on("click", function(){
            swal({
                title: 'Hasil Nishab tidak Wajib',
                text: "Anda dapat memilih zakat Infaq",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<a href="/infaq">Halaman Zakat Infaq</a>',
                cancelButtonText: 'Tidak, batalkan!',
                });
              });   
          }
      })
      /*$("#insert").on("click", function(){
            swal({
                title: 'Apakah Data Sudah Sesuai?',
                text: "Anda Dapat Mengedit Kembali",
                imageUrl: 'zakatfitrah.jpg',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Saya Yakin',
                cancelButtonText: 'Tidak, batalkan!',
                }).then((result) => {
                if (result.value) {
                    $('#addpenghasilan').submit();
                    }
          });
                });*/  
    });
  </script>

  @endsection