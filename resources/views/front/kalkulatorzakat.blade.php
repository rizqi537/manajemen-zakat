@extends('layouts.front')

@section('content')
<main id="main">

    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">
  
          <ol>
            <li><a href="/">Beranda</a></li>
            <li>Kalkulator Zakat</li>
          </ol>
          <h2>Kalkulator Zakat</h2>
  
        </div>
      </section><!-- End Breadcrumbs -->
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">
        <h2>Zakat Penghasilan</h2>
        <div class="row">

          <div class="col-lg-4">
            <form action="" method="post" role="form" id="addpenghasilan">
              @csrf
              <label for="rp">Harga Beras yang dikonsumsi</label>
              <select style="width: 200px" class="form-control" id="harga_beras" name="harga_beras">
                <option value="0" disabled="true" selected="true" id="harga_beras">-- Harga Beras --</option>
                @foreach($beras as $beras)
                    <option value="{{$beras->uang_beras}}">{{$beras->uang_beras}}</option>
                @endforeach
            </select>
              <br>
              <div class="form-group">
                <label for="rp">Penghasilan/Gaji per Bulan</label>
                <input type="text" name="gaji_bulan" class="form-control only-num" id="gaji_bulan" placeholder="0" data-rule="minlen:4" data-msg="Masukan Jumlah Jiwa"/>
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <label for="rp"><b>Wajib Membayar Zakat ? </b></label>
                <input type="text" name="wajib" class="form-control" id="wajib" placeholder="Keterangan" data-rule="minlen:4" data-msg="Masukan Jumlah Jiwa"/>
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <label for="rp"><b>Jumlah Zakat</b></label>
                <input type="text" name="jumlah_zakat" class="form-control only-num" id="jumlah_zakat" placeholder="0" data-rule="minlen:4" data-msg="Masukan Jumlah Jiwa"/>
                <div class="validate"></div>
              </div>
            </form>
          </div>
        </div>

      </div>
    </section><!-- End Contact Section -->
    <section id="contact" class="contact">
        <div class="container">
            <h2>Fidiah</h2>
          <div class="row">
  
            <div class="col-lg-7" id="insertModal">
              <form action="" method="post" id="addfidyah">
                @csrf
                <div class="form-group">
                  <label for="rp">Jumlah Hari</label>
                  <input type="text" name="jumlahhari" class="form-control jumlahhari only-num" id="jumlahhari" placeholder="Maksimal 30 Hari" data-rule="minlen:4" data-msg="Masukan Jumlah Hari" value=""/>
                  <div class="validate"></div>
                </div>
                <br>
                <div class="form-group">
                  <label for="rp">Rp.</label>
                  <input type="text" name="rupiah" class="form-control rupiah" id="rupiah" placeholder="Rp..." data-rule="minlen:4" data-msg="Pilih Jumlah Jiwa" value="45000" disabled/>
                  <div class="validate"></div>
                </div>
                <div class="form-group">
                  <label for="rp">Rp.</label>
                  <input type="text" name="totalfi" class="form-control totalfi only-num" id="totalfi" placeholder="Total" data-rule="minlen:4" data-msg="Harga Beras"/>
                  <div class="validate"></div>
                </div>
                <br>     
              </form>
            </div>
  
          </div>
  
        </div>
      </section><!-- End Contact Section -->
      <section id="contact" class="contact">
        <div class="container">
            <h2>Zakat Fitrah</h2>
          <div class="row">
  
            <div class="col-lg-7">
              <form action="" method="post" role="form" id="addfitrah">
                @csrf
                <div class="form-group">
                    <input type="text" name="harga_beras2" class="form-control only-num" id="harga_beras2" placeholder="Masukan Harga Beras yang dikonsumsi" data-rule="minlen:4" data-msg="Masukan Jumlah Jiwa"/>
                    <div class="validate"></div>
                  </div>
                <div class="form-group">
                  <input type="text" name="jumlah_jiwa2" class="form-control only-num" id="jumlah_jiwa2" placeholder="Masukan Jumlah Jiwa" data-rule="minlen:4" data-msg="Masukan Jumlah Jiwa"/>
                  <div class="validate"></div>
                </div>
                <div class="form-group">
                  <input type="text" name="totalfit" class="form-control totalfit only-num" id="totalfit" placeholder="Total Zakat Fitrah" data-rule="minlen:4" data-msg="Total Hargs Beras"/>
                  <div class="validate"></div>
                </div>
                <br>     
              </form>
            </div>
  
          </div>
  
        </div>
      </section><!-- End Contact Section -->

  </main><!-- End #main -->
<script type="text/javascript">
    $(document).ready(function(){
        $( ".only-num" ).keypress(function(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            });
            //Hitungan Zakat Penghasilan
            $("input").on("keyup", function(){
                var harga_beras = $("#harga_beras").val();
                var gaji = $("#gaji_bulan").val();
                var nishab =  harga_beras*524;
                //$("#nishab").val(nishab);
                var res = 0.025*gaji;
                //$("#jumlah_zakat").val(res);
                if (gaji>nishab){
                    $("#wajib").val('Wajib');
                    $("#jumlah_zakat").val(res); 
                }else{
                    $("#wajib").val('Tidak Wajib');
                    $("#jumlah_zakat").val('0');   
                }
            })
            //Hitungan Zakat Fidyah
            $("input").on("keyup", function(){
                var jumlahhari = $("#jumlahhari").val();
                var rupiah = $("#rupiah").val();
                var res = jumlahhari*rupiah;
            $("#totalfi").val(res)
        });
        //Hitungan Zakat Fitrah
        $("input").on("keyup", function(){
            var harga_beras2 = $("#harga_beras2").val();
            var jumlah_jiwa2 = $("#jumlah_jiwa2").val();
            var res = harga_beras2*2.5*jumlah_jiwa2;
        $("#totalfit").val(res);
      })
            });
</script>
@endsection