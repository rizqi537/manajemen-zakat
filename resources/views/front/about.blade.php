@extends('layouts.front')

@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="/">Beranda</a></li>
          <li>Tentang Kami</li>
        </ol>
        <h2>Tentang Kami</h2>

      </div>
    </section><!-- End Breadcrumbs -->

   <section id="about" class="about">
      <div class="container">

        <div class="row">
          <div class="col-lg-6">
            <img src="assets/img/gedungmiftahul.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content">
            <h3>YAYASAN MIFTAHUL FALAH AL AMAANAH</h3>
            <p class="">
            <h5>Notaris-PPAT : Sinta Rosmalia Sari, S.H., M.Kn.<br/>
                SK Nomor AHU-0028682.AH.01.04 Tahun 2015<br/></h5>
              Menyelenggarakan : 
            </p>
            <ul>
              <li><i class="icofont-check-circled"></i> MAJELIS TA'LIM</li>
              <li><i class="icofont-check-circled"></i> DINIYAH TAKMILIYAH AWALIAH</li>
              <li><i class="icofont-check-circled"></i> MENGELOLA ANAK YATIM DAN DU'AFA</li>
              <li><i class="icofont-check-circled"></i> TAMAN KANAK-KANAK AL-QUR'AN</li>
              <li><i class="icofont-check-circled"></i> TAMAN PENDIDIKAN AL-QUR'AN</li>
              <li><i class="icofont-check-circled"></i> MENERIMA TITIPAN ZAKAT, INFAQ, DAN SODAQOH (ZIS)</li>
            </ul>
            <div class="col-lg-6 pt-4 pt-lg-0 content">
            <h3 class="">Lokasi Kami</h3>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63372.22040542493!2d107.55975676411435!3d-6.91881969019277!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xff88c98384142d5b!2sYayasan%20Miftahul%20Falah%20Al-Amanah!5e0!3m2!1sid!2sid!4v1611566628589!5m2!1sid!2sid" width="365" height="265" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>

          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Counts Section ======= -->
    

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
      <div class="container">

        <div class="section-title">
          
        </div>

      </div>
    </section><!-- End Clients Section -->

    <!-- ======= Testimonials Section ======= -->
    

  </main><!-- End #main -->

@endsection