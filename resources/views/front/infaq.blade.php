@extends('layouts.front')

@section('content')

<main id="main">

    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">
  
          <ol>
            <li><a href="/">Beranda</a></li>
            <li>Zakat</li>
            <li>Infaq</li>
          </ol>
          <h2>Infaq</h2>
  
        </div>
      </section><!-- End Breadcrumbs -->
    <!-- ======= Contact Section ======= -->
<style>
div {
  margin-bottom: 15px;
  padding: 4px 12px;
}

.info {
  background-color: #e7f3fe;
  border-left: 6px solid #2196F3;
}
</style>
    <section id="contact" class="contact">
      <div class="container">

        <div class="row">

          <div class="col-lg-7">
            <form action="{{route('infaq.store')}}" method="post" role="form" id="addinfaq">
              @csrf
              <br>
              <div class="form-group">
                <label for="rp">Rp.</label>
                <input type="text" name="totalfaq" class="form-control totalfaq only-num" id="totalfaq" placeholder="Jumlah Zakat Infaq" data-rule="minlen:4" data-msg="Total Infaq"/>
                <span class="validate">
                  <strong style="color:red">{{ $errors->first('totalfaq') }}</strong>
              </span>
              </div>
              <br>     
              <div class="form-group">
                  <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Lengkap" data-rule="minlen:4" data-msg="Masukan nama lengkap Anda" />
                  <span class="validate">
                    <strong style="color:red">{{ $errors->first('nama') }}</strong>
                </span>
                </div>
                <div class="form-group">
                    <input type="text" name="nohp" class="form-control only-num" id="nohp" placeholder="Nomor Telepon" data-rule="minlen:4" data-msg="Masukan nomor Telepon Anda" />
                    <div class="validate"></div>
                  </div>
                  <div class="form-group">
                    <input type="text" name="email" class="form-control" id="email" placeholder="Alamat E-mail" data-rule="minlen:4" data-msg="Masukan Alamat E-mail Anda" />
                    <span class="validate">
                      <strong style="color:red">{{ $errors->first('email') }}</strong>
                  </span>
                  </div>
                  <div class="col-lg-12 mt-4 mt-lg-0">
                  <div class="info">
                    <p><strong></strong> Sebelum melanjutkan ke menu pembayaran sebaiknya telah melakukan pembayaran ke rekening Bank BJB Syariah A.n <b>Miftahul Falah Al Amaanah</b> No. Rek: <b>0113295767101</b></p>
                  </div>
                </div>
                  <input type="button" name="insert" id="insert" value="Lakukan Pembayaran" class="btn btn-primary m-t-15 waves-effect">
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <script type="text/javascript">
    $(document).ready(function(){
      $("#insert").on("click", function(){
            swal({
                title: 'Apakah Data Sudah Sesuai?',
                text: "Anda Dapat Mengedit Kembali",
                imageUrl: 'zakatfidyah.jpg',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Saya Yakin',
                cancelButtonText: 'Tidak, batalkan!',
                }).then((result) => {
                if (result.value) {
                    $('#addinfaq').submit();
                    }
          });
                });

      
    });
  </script>

@endsection