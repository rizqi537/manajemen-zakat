@extends('layouts.front')

@section('content')
    <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <!-- Slide 1 -->
          <div class="carousel-item active" style="background: url(assets/img/slide/zakat1.jpg)">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown">Lembaga Amil Zakat Infak Dan Sedekah <span><br/>Yayasan Miftahul Falah Al Amaanah</span></h2>
                <p class="animate__animated animate__fadeInUp">Kami adalah Lembaga Amil yang dibentuk untuk mengabdikan diri bagi umat dan berfokus pada penyaluran Zakat Infak dan Sedekah sesuai dengan syariat Islam. </p>
                <!--a href="" class="btn-get-started animate__animated animate__fadeInUp">Read More</a-->
              </div>
            </div>
          </div>

          <!-- Slide 2 -->
          <div class="carousel-item" style="background: url(assets/img/slide/telapak.jpg)">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated fanimate__adeInDown">Zakat<span>Fitrah</span></h2>
                <p class="animate__animated animate__fadeInUp">Zakat fitrah menurut Ibnu Quutaibah adalah zakat (shadaqah) jiwa, istilah itu diambil dari kata fitrah yang merupakan asal dari kejadian.
                      </br> Zakat fitrah dikenakan kepada setiap individu muslim tanpa memandang usia dan harta yang dimiliki. Zakat ini dikeluarkan pada akhir ramadhan sebelum shalat Hari Raya Idul Fitri.
                      </br>Hal ini didasari hadist nabi Muhammad SAW: “Rasulullah SAW telah memfardukan zakat fitrah satu sha’ atas anak kurma atau gandum kepada budak, orang merdeka, laki-laki dan perempuan dari seluruh kaum muslimin. Dan beliau perintahkan supaya dikeluarakan sebelum manusia keluar untuk shalat (Ied)” (H.R Bukhari).</p>
                <!--a href="" class="btn-get-started animate__animated animate__fadeInUp">Read More</a-->
              </div>
            </div>
          </div>

          <!-- Slide 3 -->
          <div class="carousel-item" style="background: url(assets/img/slide/telapak.jpg)">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown">Zakat <span>Penghasilan</span></h2>
                <p class="animate__animated animate__fadeInUp">Adalah zakat atas semua bentuk gaji, honor, insentif, biaya, upah, bonus, hadiah dan apapun jenis pemasukan dari hasil bekerja bila telah mencapai nisab.
                      </br>Menurut Yusuf Qorodhowi, sangat dianjurkan untuk menghitung zakat dari pendapatan kasar (brutto), untuk lebih menjaga kehati-hatian.
											</br>Nisab sebesar 5 wasaq / 652,8 kg gabah setara 520 kg beras. Besar zakat profesi yaitu 2,5 %.</p>
                <!--a href="" class="btn-get-started animate__animated animate__fadeInUp">Read More</a-->
              </div>
            </div>
          </div>

           <!-- Slide 4 -->
           <div class="carousel-item" style="background: url(assets/img/slide/telapak.jpg)">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown">Infak &<span> Sedekah</span></h2>
                <p class="animate__animated animate__fadeInUp">Adalah harta yang dikeluarkan oleh seseorang atau badan usaha di luar zakat untuk kemaslahatan umum.
                      </br>Infak dan Sedekah tidak mengenal nisab atau jumlah harta yang ditentukan secara hukum syariat. 
											</br>Bentuk infak dan sedekah tidak ditentukan, jadi bisa berupa uang maupun barang.Bahkan disebutkan dalam sebuah hadist : “setiap ruas yang aktif dari kamu itu harus disedekahi. Maka setiap tasbih itu nilainya sedekah, setiap tahmid sedekah, setiap tahlil itu sedekah, setiap takbir itu sedekah dan amar makruf nahi munkar itu juga sedekah.”(H.R Muslim).</p>
                <!--a href="" class="btn-get-started animate__animated animate__fadeInUp">Read More</a-->
              </div>
            </div>
          </div>
           <!-- Slide 5 -->
           <div class="carousel-item" style="background: url(assets/img/slide/telapak.jpg)">
            <div class="carousel-container">
              <div class="carousel-content">
                <h2 class="animate__animated animate__fadeInDown"><span>Fidiah</span></h2>
                <p class="animate__animated animate__fadeInUp">Bagi beberapa orang yang tidak mampu menjalankan ibadah puasa dengan kriteria tertentu, diperbolehkan tidak berpuasa serta tidak harus menggantinya di lain waktu. Namun, sebagai gantinya diwajibkan untuk membayar fidiah.
                      </br>Di jelaskan dalam surat Al-Baqarah ayat 184. ”(yaitu) dalam beberapa hari yang tertentu. Maka barangsiapa diantara kamu ada yang sakit atau dalam perjalanan (lalu ia berbuka), maka (wajiblah baginya berpuasa) sebanyak hari yang ditinggalkan itu pada hari-hari yang lain. Dan wajib bagi orang-orang yang berat menjalankannya (jika mereka tidak berpuasa) membayar fidiah, (yaitu): memberi makan seorang miskin. Barangsiapa yang dengan kerelaan hati mengerjakan kebajikan, maka itulah yang lebih baik baginya. Dan berpuasa lebih baik bagimu jika kamu mengetahui.” (Q.S. Al Baqarah: 184)
											</br>Fidiah yang harus dikeluarkan sebesar 2 mud atau setara 1/2 sha' gandum. (Jika 1 sha' setara 4 mud = sekitar 3 kg, maka 1/2 sha' berarti sekitar 1,5 kg).</p>
                <!--a href="" class="btn-get-started animate__animated animate__fadeInUp">Read More</a-->
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Featured Section ======= -->
    <section id="featured" class="featured">
      <div class="container">

        <div class="row">
          <div class="col-lg-3">
            <div class="icon-box">
              <i class="icofont-tasks-alt"></i>
              <h3><a href="/zakatfitrah">Tunaikan Zakat Fitrah</a></h3>
              <p>Dikeluarkan pada akhir ramadhan sebelum shalat Hari Raya Idul Fitri dengan ketentuan 2.5Kg beras konsumsi/jiwa.</p>
            </div>
          </div>
          <div class="col-lg-3 mt-4 mt-lg-0">
            <div class="icon-box">
              <i class="icofont-tasks-alt"></i>
              <h3><a href="/form-penghasilan">Tunaikan Zakat Penghasilan</a></h3>
              <p>Besaran 2.5% dari pendapatan kotor(bruto) dengan nisab 520Kg Beras.</p>
            </div>
          </div>
          <div class="col-lg-3 mt-4 mt-lg-0">
            <div class="icon-box">
              <i class="icofont-tasks-alt"></i>
              <h3><a href="/zakatfidyah">Tunaikan Fidiah</a></h3>
              <p>Ketentuan jumlah Fidiah adalah Rp. 45.000/hari Sumber: </strong><a href=https://baznas.go.id/fidyahBAZNAS>BAZNAS.</p>
            </div>
          </div>
          <div class="col-lg-3 mt-4 mt-lg-0">
            <div class="icon-box">
              <i class="icofont-tasks-alt"></i>
              <h3><a href="/zakatinfaq">Tunaikan Infak</a></h3>
              <p>Seluruh dana Infak akan disalurkan untuk pembangunan gedung baru Yayasan Miftahul Falah Al Amaanah.</p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Featured Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row">
          <div class="col-lg-6">
            <img src="assets/img/gedungmiftahul.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content">
            <h3>YAYASAN MIFTAHUL FALAH AL AMAANAH</h3>
            <p class="">
            <h5>Notaris-PPAT : Sinta Rosmalia Sari, S.H., M.Kn.<br/>
                SK Nomor AHU-0028682.AH.01.04 Tahun 2015<br/></h5>
              Menyelenggarakan : 
            </p>
            <ul>
              <li><i class="icofont-check-circled"></i> MAJELIS TA'LIM</li>
              <li><i class="icofont-check-circled"></i> DINIYAH TAKMILIYAH AWALIAH</li>
              <li><i class="icofont-check-circled"></i> MENGELOLA ANAK YATIM DAN DU'AFA</li>
              <li><i class="icofont-check-circled"></i> TAMAN KANAK-KANAK AL-QUR'AN</li>
              <li><i class="icofont-check-circled"></i> TAMAN PENDIDIKAN AL-QUR'AN</li>
              <li><i class="icofont-check-circled"></i> MENERIMA TITIPAN ZAKAT, INFAQ, DAN SODAQOH (ZIS)</li>
            </ul>
            <div class="col-lg-6 pt-4 pt-lg-0 content">
            <h3 class="">Lokasi Kami</h3>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63372.22040542493!2d107.55975676411435!3d-6.91881969019277!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xff88c98384142d5b!2sYayasan%20Miftahul%20Falah%20Al-Amanah!5e0!3m2!1sid!2sid!4v1611566628589!5m2!1sid!2sid" width="365" height="265" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
            <p>
            
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Artikel</h2>
        </div>
        @if(count($artikels) >0)
        <div class="row">
          @foreach($artikels as $artikel)
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <img src="{{asset("uploadphoto/$artikel->photo")}}" title="" width="300" height="300"></a>
              <h4><a href="/view-artikel/{{$artikel->id}}">{{$artikel->title}}</a></h4>
            </div>
          </div>
          @endforeach
        </div>
        @else
        <p>There's no article to display</p>
        @endif
      </div>

    </section><!-- End Services Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
      <div class="container">


        

      </div>
    </section><!-- End Clients Section -->

  </main><!-- End #main -->
@endsection