@extends('layouts.app')

@section('title', 'Data List Jenis Mustahiq')
@section('content')

<div class="block-header">
    <h2>DATA JENIS MUSTAHIQ</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-11">
                        <h2>
                            DATA JENIS MUSTAHIQ
                        </h2>
                    </div>
                </div>
            </div>
            <div class="body">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="tbjenismustahiq">
                        <thead>
                            <tr>
                                <th>JENIS</th>
                                <th>KETERANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function(){
        var table = $('#tbjenismustahiq').DataTable({
            dom: 'Bfrtip',
        responsive: true,
        processing: true,
        serverSide: true,
        buttons: [
            {
                extend: 'print',
                footer: false,
                exportOptions: {
                    columns: [0,1]
                }
            },
            {
                extend: 'pdf',
                footer: false,
                exportOptions: {
                    columns: [0,1]
                }
                
            },
            {
                extend: 'excel',
                footer: false,
                exportOptions: {
                    columns: [0,1]
                }
            } 
        ],
        ajax: {
            url: '{{ url("list-jenismustahiq") }}'
        },
        columns: [
        {data: 'jenis', name: 'jenis'},
        {data: 'keterangan', name: 'keterangan'},
        //{data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        });
    });
</script>
@endsection