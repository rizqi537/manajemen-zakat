@extends('layouts.app')

@section('title', 'Data Zakat Fidyah')
@section('content')
<div class="block-header">
            <h2>DATA FIDIAH</h2>
        </div>
        <div class="row clearfix">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        		<div class="card">
        			<div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-11">
                                <h2>
                                    DATA FIDIAH
                                </h2>  
                            </div>
                        </div>
        			</div>
                    <div class="body">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="tbzakatfidyah">
                                <thead>
                                    <tr>
                                        <th>JUMLAH HARI</th>
                                        <th>TOTAL FIDIAH</th>
                                        <th>NAMA</th>
                                        <th>NO.TELEPON</th>
                                        <th>E-MAIL</th>
                                        <th>KETERANGAN</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
        		</div>
        	</div>
        </div>
        
        <script>
            jQuery(document).ready(function(){
                var table = $('#tbzakatfidyah').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    buttons: [
                        {
                            extend: 'print',
                            footer: false,
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6]
                            }
                        },
                        {
                            extend: 'pdf',
                            footer: false,
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6]
                            }
                            
                        },
                        {
                            extend: 'excel',
                            footer: false,
                            exportOptions: {
                                columns: [0,1,2,3,4,5,6]
                            }
                        } 
                    ],
                    ajax: {
                        url: '{{ url("list-fidyah") }}'
                    },
                    columns: [
                    {data: 'jumlahhari', name: 'jumlahhari'},
                    {data: 'totalfi', name: 'totalfi'},
                    {data: 'nama', name: 'nama'},
                    {data: 'nohp', name: 'nohp'},
                    {data: 'email', name: 'email'},
                    {data: 'keterangan', name: 'keterangan'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
                });
                
                $("#tbzakatfidyah").on("click", "#apus", function(e){
                    var csrf_token = $('meta[name="csrf-token"]').attr("content");
                    var id = $(this).data("value");
                    swal({
                        title: 'Apakah Kamu Yakin Ingin Dihapus?',
                        text: "Data Tidak Bisa Dikembalikan",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Saya Yakin',
                        cancelButtonText: 'Tidak, batalkan!',
                        }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                url: "{{ url('fidyah/delete') }}"+ '/' + id,
                                type: "POST",
                                data : {'_method' : 'DELETE', '_token' : csrf_token},
                                success : function(){
                                    table.ajax.reload();
                                }
                            })
                        }
					})
                });

            });

        </script>
        
@endsection