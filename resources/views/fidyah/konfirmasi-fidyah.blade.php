@extends('layouts.front')

@section('content')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="/">Home</a></li>
          <li>Konfirmasi Pembayaran Fidiah</li>
        </ol>
        <h2>Konfirmasi Pembayaran Fidiah</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
      <div class="container">

          <form action="{{route('fidyah.bukti', $fidyah->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            {{  method_field('PATCH') }}
              <label for="bukti">Masukan Bukti Transfer:</label>
              <input type="file" id="bukti" name="bukti">
              <input type="submit">
          </form>
          <br>

        <table class="table table-bordered" style="width: 80%">
            <tr>
                <th>Jumlah Hari :</th>
                <td>{{$fidyah->jumlahhari}}</td>
            </tr>
            <tr>
                <th>Total Zakat Fidyah :</th>
                <td>{{"Rp. ".number_format($fidyah->totalfi,0,'',',').',-'}}</td>
            </tr>
            <tr>
               <th>Pembayaran Dilakukan ke rekening Bank BJB A.n <b>Miftahul Falah Al Amaanah</b></th>
                <td><b>0113295767101</b></td>
            </tr>     
        </table>
        
        <table class="table table-bordered" style="width: 80%">
            <tr>
                <th>Nama Lengkap :</th>
                <td>{{$fidyah->nama}}</td>
            </tr>
            <tr>
                <th>No Telepon :</th>
                <td>{{$fidyah->nohp}}</td>
            </tr>
            <tr>
                <th>Email :</th>
                <td>{{$fidyah->email}}</td>
            </tr>  
        </table>

      </div>
    </section><!-- End Pricing Section -->

  </main><!-- End #main -->

  @endsection