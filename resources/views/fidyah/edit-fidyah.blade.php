@extends('layouts.app')

@section('title',"Edit Transaksi Fidyah")
@section('content')
<div class="block-header">
    <h2>DATA FIDIAH</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<div class="card">
        	<div class="header">
                <h2>
                    DATA FIDIAH
                </h2>
            </div>
                    <div class="body">
                    	<form class="form-vertical" id="myform" action="{{route('fidyah.update', $fidyah)}}" method="POST" enctype="multipart/form-data">
                    	<h2 class="card-inside-title">DATA FIDIAH</h2>
							@csrf
                    		{{  method_field('PATCH') }}
							<input type="hidden" name="_idf" value="{{base64_encode($fidyah->id)}}">
                    		<div class="form-group form-float">
                    			<div class="form-line">
                    				<input type="text" name="jumlahhari" id="jumlahhari" class="form-control only-num" required="" value="{{$fidyah->jumlahhari}}">
                    				<label class="form-label">Jumlah Hari</label>
                    			</div>
                    		</div>
                    		<div class="form-group form-float">
                    			<div class="form-line">
                    				<input type="email" id="rupiah" name="rupiah" class="form-control" value="45000" disabled>
                    				<label class="form-label">Rupiah</label>
                    			</div>
                    		</div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="totalfi" name="totalfi" class="form-control only-num" value="{{$fidyah->totalfi}}">
                                    <label class="form-label">Total Fidiah</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="nama" name="nama" class="form-control" value="{{$fidyah->nama}}">
                                    <label class="form-label">Nama Lengkap</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="nohp" name="nohp" class="form-control only-num" value="{{$fidyah->nohp}}">
                                    <label class="form-label">Nomor Telepon</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="email" name="email" class="form-control" value="{{$fidyah->email}}">
                                    <label class="form-label">Email</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label class="form-label">Bukti Transfer</label>
                                <input type="file" id="bukti" name="bukti">
                                <div class="form-line">
                                    <img src="{{asset("uploadbukti/$fidyah->bukti")}}" alt="" title="" width="500" height="500"></a>
                                </div>
                            </div>
                    		<div class="row clearfix">
                                <div class="">
                                <input type="button" name="insert" id="insert" value="Ubah" class="btn btn-primary m-t-15 waves-effect">
                                <input type="reset" class="btn btn-primary m-t-15 waves-effect" value="RESET">
                                </div>
                            </div>
                    	</form>
                    </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (){
        $( ".only-num" ).keypress(function(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;
				return true;
			});
			$( ".dec-num" ).keypress(function(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
					return false;
				return true;
			});
            $("input").on("keyup", function(){
                var jumlahhari = $("#jumlahhari").val();
                var rupiah = $("#rupiah").val();
                var res = jumlahhari*rupiah;
                $("#totalfi").val(res)
            });
            $("#insert").click(function(){
				swal({
					title: 'Apakah Datanya Sudah Sesuai?',
					text: "Pastikan Data Yang Anda Berikan Sesuai",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Sudah Sesuai',
 					cancelButtonText: 'Tidak, batalkan!',
					}).then((result) => {
					if (result.value) {
						$('#myform').submit();
					}
					})
			});
    });
    </script>
@endsection