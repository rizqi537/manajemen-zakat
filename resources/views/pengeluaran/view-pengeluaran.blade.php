@extends('layouts.app')

@section('title',"Edit Transaksi Fitrah")
@section('content')

<div class="block-header">
    <h2>DATA PENGELUARAN</h2>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<div class="card">
        	<div class="header">
                <h2>
                    VIEW PENGELUARAN
                </h2>
            </div>
                    <div class="body">
                    	<form class="form-vertical" id="myform" action="" method="POST" enctype="multipart/form-data">
                    	<h2 class="card-inside-title">DATA ZAKAT fitrah</h2>
                        <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="totalfit" name="totalfit" class="form-control only-num" value="{{ optional($pengeluaran->typezakat)->nama_zakat}}" disabled>
                                    <label class="form-label">Tipe Zakat</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="totalfit" name="totalfit" class="form-control only-num" value="{{ optional($pengeluaran->jenismustahiq)->jenis }}" disabled>
                                    <label class="form-label">Tipe Mustahiq</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="nama" name="nama" class="form-control" value="{{$pengeluaran->jumlah}}" disabled>
                                    <label class="form-label">Jumlah</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="nohp" name="nohp" class="form-control only-num" value="{{$pengeluaran->keterangan}}" disabled>
                                    <label class="form-label">Keterangan</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label class="form-label">Bukti</label>
                                <input type="file" id="bukti" name="bukti">
                                <div class="form-line">
                                    <img src="{{asset("uploadpengeluaran/$pengeluaran->bukti")}}" alt="" title="" width="500" height="500"></a>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="email" name="email" class="form-control" value="{{$pengeluaran->nama}}" disabled>
                                    <label class="form-label">Nama</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="email" name="email" class="form-control" value="{{$pengeluaran->alamat}}" disabled>
                                    <label class="form-label">Alamat</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="email" name="email" class="form-control" value="{{$pengeluaran->nik}}" disabled>
                                    <label class="form-label">NIK</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="email" name="email" class="form-control" value="{{$pengeluaran->pekerjaan}}" disabled>
                                    <label class="form-label">Pekerjaan</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="email" name="email" class="form-control" value="{{$pengeluaran->status}}" disabled>
                                    <label class="form-label">Status</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="email" name="email" class="form-control" value="{{$pengeluaran->jumlah_mustahik}}" disabled>
                                    <label class="form-label">Jumlah Mustahik</label>
                                </div>
                            </div>
                    		<div class="row clearfix">
                                
                            </div>
                    	</form>
                    </div>
        </div>
    </div>
</div>
<script>
    
</script>
@endsection